package com.sport.redis;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class BaseRedis<T> {
	
    @Autowired
    protected RedisTemplate<String, String> redisTemplate;
    
    protected Class<T> entityClass;
    
    @SuppressWarnings("unchecked")
	public BaseRedis() {
		this.entityClass = getSuperClassGenricType(getClass(), 0);
	}
    
    @SuppressWarnings("rawtypes")
	public static Class getSuperClassGenricType(Class clazz, int index) {
		Type genType = clazz.getGenericSuperclass();

		if (!(genType instanceof ParameterizedType)) {
			return Object.class;
		}
		
		Type[] params = ((ParameterizedType) genType).getActualTypeArguments();

		if ((index >= params.length) || (index < 0)) {
			return Object.class;
		}
		if (!(params[index] instanceof Class)) {
			return Object.class;
		}

		return (Class) params[index];
	}

    /**
     * 
     * @param key  redis key
     * @param time 单位: 分钟
     * @param t    对象, 要求可序列化
     */
    public void add(String key, Long time, T t) {
        Gson gson = new Gson();
        redisTemplate.opsForValue().set(key, gson.toJson(t), time, TimeUnit.MINUTES);
    }

    /**
     * 
     * @param key  redis key
     * @param time 单位: 分钟
     * @param list 对象列表, 对象要求可序列化
     */
    public void add(String key, Long time, List<T> list) {
        Gson gson = new Gson();
        redisTemplate.opsForValue().set(key, gson.toJson(list), time, TimeUnit.MINUTES);
    }

    public T get(String key) {
        Gson gson = new Gson();
        T user = null;
        String json = redisTemplate.opsForValue().get(key);
        if(!StringUtils.isEmpty(json))
            user = gson.fromJson(json, entityClass);
        return user;
    }

    public List<T> getList(String key) {
        Gson gson = new Gson();
        List<T> ts = null;
        String listJson = redisTemplate.opsForValue().get(key);
        if(!StringUtils.isEmpty(listJson))
            ts = gson.fromJson(listJson, new TypeToken<List<T>>(){}.getType());
        return ts;
    }

    public void delete(String key){
        redisTemplate.opsForValue().getOperations().delete(key);
    }
}
