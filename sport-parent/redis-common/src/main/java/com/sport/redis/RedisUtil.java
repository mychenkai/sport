package com.sport.redis;
import redis.clients.jedis.Jedis;

public class RedisUtil {
    //往redis中放值
    public static void setPool(String key ,String value){
        Jedis jedis=null;
        try {
            jedis = RedisPool.getResource();
            jedis.set(key, value);
        }finally {
            if (null!=jedis){
                jedis.close();
                jedis=null;
            }
        }
    }
    //根据key移除redis中的数据
    public static void delPool(String key){
        Jedis jedis=null;
        try {
            jedis = RedisPool.getResource();
            jedis.del(key);
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            if (null!=jedis){
                jedis.close();
                jedis=null;
            }
        }
    }
    //设置过期时间
    public static void exits(String key ,int seconds){
        Jedis jedis=null;
        try {
            jedis = RedisPool.getResource();
            jedis.expire(key, seconds);
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            if (null!=jedis){
                jedis.close();
                jedis=null;
            }
        }
    }
    //从redis中取值
    public static String getPool(String key){
        Jedis jedis=null;
        String result="";
        try {
             jedis = RedisPool.getResource();
             result = jedis.get(key);
        }catch (Exception e){
            e.printStackTrace();
            return result;
        } finally {
            if (null!=jedis){
                jedis.close();
                jedis=null;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        //setPool("stuName","lisi");
        String stuName = getPool("stuName");
        System.out.println(stuName);
    }

    //判断键值是否存在，存在的话并设置过期时间
    public static boolean setNxExpire(String key,String value,int seconds) {
        Jedis jedis=null;
        try {
            jedis = RedisPool.getResource();
            //成功返回1，失败返回0
            Long flag = jedis.setnx(key, value);
            if (flag==1){
                //证明是第一次登陆
                //设置过期时间
                jedis.expire(key, seconds);
                return true;
            }else {
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException();
        } finally {
            if (null!=jedis){
                jedis.close();
                jedis=null;
            }
        }
    }
    //根据key在senconds指定时间内进行递增并返回递增后的数
    public static long incrExpire(String key,int senconds) {
        Jedis jedis=null;
        try {
            jedis = RedisPool.getResource();
            Long count = jedis.incr(key);
            if (count==1){
                jedis.expire(key, senconds);
            }
            return count;
        }finally {
            if (null!=jedis){
                jedis.close();
                jedis=null;
            }
        }
    }
}
