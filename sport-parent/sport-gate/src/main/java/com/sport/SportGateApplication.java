package com.sport;

import com.sport.gate.filter.SafeFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@EnableHystrix
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
@EnableZuulProxy
@ComponentScan(basePackages = "com.sport")
public class SportGateApplication {

    public static void main(String[] args) {
        SpringApplication.run(SportGateApplication.class, args);
    }

    @Bean
    public SafeFilter safeFilter() {
        return new SafeFilter();
    }
}
