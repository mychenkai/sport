package com.sport.gate.filter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Enumeration;

import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.sport.gate.biz.UserBiz;
import com.sport.user.vo.UserTokenVo;

@Component
@WebFilter(urlPatterns = { "/*" })
@Order(value = -1)
public class PostAccessFilter extends ZuulFilter {

	private static Logger log = LoggerFactory.getLogger(PostAccessFilter.class);

	@Autowired
	private UserBiz userBiz;

	@Value("${gate.ignore.startWith}")
    private String startWith;

    @Value("${gate.ignore.uris}")
    private String uris;

	@Override
	public String filterType() {
		return "post";
	}

	@Override
	public int filterOrder() {
		return 0;
	}

	@Override
	public boolean shouldFilter() {
		return false;
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		HttpServletResponse response = ctx.getResponse();
		Collection<String> headers = response.getHeaderNames();
		log.info("<<<===============================================");
		log.info(String.format("%-30s : %s",  "response.getStatus()", response.getStatus()));
		log.info(String.format("%-30s : %s",  "response.getBufferSize()", response.getBufferSize()));
		for(String header : headers) {
			String value = request.getHeader(header);
			log.info(String.format("%-30s : %s",  header, value));
		}
		log.info("===============================================>>>");

//         System.out.println("拦截请求: "+httpServletRequest.getServletPath());
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "0");
        response.setHeader("Access-Control-Allow-Headers", "Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With,userId,token");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("XDomainRequestAllowed","1");
		return null;
	}
}
