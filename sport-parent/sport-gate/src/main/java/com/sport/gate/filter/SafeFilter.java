package com.sport.gate.filter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.Enumeration;

import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;
import com.sport.common.JsonResult;
import com.sport.common.SystemEnum;
import com.sport.common.safeFilter.WebContext;
import com.sport.redis.RedisUtil;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

@Component
@WebFilter(urlPatterns = { "/*" })
@Order(value = -1)
public class SafeFilter extends ZuulFilter {

	private static Logger log = LoggerFactory.getLogger(SafeFilter.class);

	@Value("${gate.ignore.startWith}")
    private String startWith;

    @Value("${gate.ignore.uris}")
    private String uris;

	@Override
	public String filterType() {
		return "pre";
	}

	@Override
	public int filterOrder() {
		return 0;
	}

	@Override
	public boolean shouldFilter() {
		return false;
	}


	//设置过期时间
	private  static final long EXPIRE=60*1000;
	@Override
	public Object run() {
		///获取request
        RequestContext currentContext = RequestContext.getCurrentContext();
        HttpServletRequest request = currentContext.getRequest();
		//获取头信息
		String appKey = request.getHeader("appKey");
		String time = request.getHeader("time");
		String sign = request.getHeader("sign");
		String nonce = request.getHeader("nonce");
		JsonResult jsonResult = new JsonResult(JsonResult.ERROR_STATUS,JsonResult.ERROR_MESSAGE);
		//验证头信息
		if (StringUtils.isEmpty(appKey)||StringUtils.isEmpty(time)||StringUtils.isEmpty(sign)){
			jsonResult.setMessage(SystemEnum.HEARD_INFO_MISS.getMessage());
            bulidResponse(currentContext, jsonResult);
			return null;
		}
		//验证是否超时
		long requestTime = Long.parseLong(time);
		if (requestTime+EXPIRE<System.currentTimeMillis()){
			jsonResult.setMessage(SystemEnum.TOKEN_TIME_OUT.getMessage());
			bulidResponse(currentContext, jsonResult);
			return null;
		}
		//验证nonce
		boolean success = RedisUtil.setNxExpire(nonce,"1",60);
		if (!success){
			jsonResult.setMessage(SystemEnum.ERROR_IS_ATTACK.getMessage());
			bulidResponse(currentContext, jsonResult);
			return null;
		}
		/*//验证appsecret是否存在
		String appSecret = safeService.findAppSecret(appKey);
		if (StringUtils.isEmpty(appSecret)){
			jsonResult.setMessage(SystemEnum.APPSECRET_IS_EMPTY+"");
			return jsonResult;
		}*/
		/*//判断签名是否正确
		String checkSum = CheckSumBuilder.getCheckSum(appSecret, nonce, time);
		if (!checkSum.equals(sign)){
			jsonResult.setMessage(SystemEnum.SIGN_IS_ERROR+"");
			return jsonResult;
		}*/
		/*//合法用户限流
		MethodSignature signature = (MethodSignature) pjp.getSignature();
		Method method = signature.getMethod();
		if (method.isAnnotationPresent(Limit.class)){
			Limit annotation = method.getAnnotation(Limit.class);
			int maxCount = annotation.maxCount();
			int seconds = annotation.seconds();
			//调用工具类的方法
			String key=appKey+"_"+request.getMethod()+"_"+request.getRequestURI();
			long value = RedisUtil.incrExpire(key, seconds);
			if (value>maxCount){
				return ServerResponse.error(SystemEnum.ERROR_IS_LIMIT);
			}
		}
		Object proceed=null;
		try {
			proceed = pjp.proceed();
		} catch (Throwable throwable) {
			throwable.printStackTrace();
			return ServerResponse.error();
		}
		return proceed;*/
		return null;
	}

    private void bulidResponse(RequestContext currentContext, JsonResult error) {
        Gson gson =new Gson();
        String result = gson.toJson(error);
        //设置编码格式和响应格式
        currentContext.getResponse().setContentType("application/json;charset=utf-8");
        //设置响应内容
        currentContext.setResponseBody(result);
        //禁止路由
        currentContext.setSendZuulResponse(false);
    }

	private void setHeaderData(RequestContext ctx, Object token) {

	}

	private boolean isStartWith(String requestUri) {
        boolean flag = false;
        for (String s : startWith.split(",")) {
            if (requestUri.startsWith(s))
                return true;
        }
        return flag;
    }

	 private boolean isContains(String requestUri) {
	        boolean flag = false;
	        for (String s : uris.split(",")) {
	            if (requestUri.contains(s))
	                return true;
	        }
	        return flag;
	    }

}
