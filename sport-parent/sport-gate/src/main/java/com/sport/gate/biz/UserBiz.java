package com.sport.gate.biz;

import com.sport.user.facade.UserFacade;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * <pre>项目名称：
 * 接口名称：UserBiz
 * 接口描述：
 * 创建人：陈凯 Mr_Chen__K@163.com
 * 创建时间：2019/8/15 15:41
 * 修改人：陈凯 Mr_Chen__K@163.com
 * 修改时间：2019/8/15 15:41
 * 修改备注：
 * @version </pre>
 */
@FeignClient(name="user-service-api")
public interface UserBiz extends UserFacade {

}
