package com.sport.gate.filter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;

import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import com.sport.gate.biz.UserBiz;
import com.sport.user.vo.UserTokenVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;


@Component
@WebFilter(urlPatterns = { "/*" })
@Order(value = -1)
public class AccessFilter extends ZuulFilter {

	private static Logger log = LoggerFactory.getLogger(AccessFilter.class);

	@Autowired
	private UserBiz userBiz;

	@Value("${gate.ignore.startWith}")
    private String startWith;

    @Value("${gate.ignore.uris}")
    private String uris;

	@Override
	public String filterType() {
		return "pre";
	}

	@Override
	public int filterOrder() {
		return 0;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		Enumeration<String> headers = request.getHeaderNames();
		log.info("<<<===============================================");
		log.info(String.format("%-30s : %s",  "request.getMethod()", request.getMethod()));
		log.info(String.format("%-30s : %s",  "request.getRequestURI()", request.getRequestURI()));
		log.info(String.format("%-30s : %s",  "request.getQueryString()", request.getQueryString()));
		log.info(String.format("%-30s : %s",  "request.getContentLength()", request.getContentLength()));
		while(headers.hasMoreElements()) {
			String headerName = headers.nextElement();
			String value = request.getHeader(headerName);
			log.info(String.format("%-30s : %s",  headerName, value));
		}
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
			StringBuffer sbStr = new StringBuffer();
			String inputLine;
			try {
				while ((inputLine = reader.readLine()) != null) {
					sbStr.append(inputLine);
				}
				reader.close();
			} catch (IOException e) {
				System.out.println("IOException: " + e);
			}
			log.info("Request Body:" + sbStr.toString());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		log.info("===============================================>>>");


		final String requestUri = request.getRequestURI();

		//不需要认证的地址
		if (isStartWith(requestUri) || isContains(requestUri)) {
			Object token = request.getParameter("token");
			if (token == null) {
				token = request.getHeader("token");
			}
			if (token != null) {
//				log.info("不需要认证的地址: 开始验证Token=====================>>>");
				if (userBiz.validateToken(token.toString())) {
					setHeaderData(ctx, token);
				}
			}
            return null;
		}

		log.info("send {} request to {}", request.getMethod(), request.getRequestURL().toString());
		Object token = request.getParameter("token");
		if (token == null) {
			token = request.getHeader("token");
		}
		if (token == null) {
			log.warn("access token is empty");
			ctx.setSendZuulResponse(false);
			ctx.setResponseStatusCode(401);
			ctx.setResponseBody("{\"result\":\"token为空!\"}");
			ctx.getResponse().setContentType("text/json;charset=UTF-8");
			return null;
		} else {
			if (userBiz.validateToken(token.toString())) {
				setHeaderData(ctx, token);
			} else {
				System.out.println("===========================");
				log.warn("access token is error");
				ctx.setSendZuulResponse(false);
				ctx.setResponseStatusCode(401);
				ctx.setResponseBody("{\"message\":\"token错误!\",\"status\":\"error\"}");
				ctx.getResponse().setContentType("text/json;charset=UTF-8");
				return null;
			}
		}
		log.info("token ok");
		return null;
	}

	private void setHeaderData(RequestContext ctx, Object token) {
		try{
			UserTokenVo userTokenVo = userBiz.getUserToken(token.toString());

			ctx.addZuulRequestHeader("userid", userTokenVo.getUserId().toString());
			ctx.addZuulRequestHeader("authorization", Base64Utils.encodeToString(userTokenVo.getUsername().getBytes()));
			ctx.addZuulRequestHeader("authorization_info", Base64Utils.encodeToString(userTokenVo.toJSON().getBytes()));
			ctx.addZuulRequestHeader("token",token.toString());
		}catch(Exception ex) {
			ctx.setSendZuulResponse(false);
			ctx.setResponseStatusCode(401);
			ctx.setResponseBody("{\"message\":\"token setHeaderData错误!\",\"status\":\"error\"}");
			ctx.getResponse().setContentType("text/json;charset=UTF-8");
		}
	}

	private boolean isStartWith(String requestUri) {
        boolean flag = false;
        for (String s : startWith.split(",")) {
            if (requestUri.startsWith(s))
                return true;
        }
        return flag;
    }

	 private boolean isContains(String requestUri) {
	        boolean flag = false;
	        for (String s : uris.split(",")) {
	            if (requestUri.contains(s))
	                return true;
	        }
	        return flag;
	    }

}
