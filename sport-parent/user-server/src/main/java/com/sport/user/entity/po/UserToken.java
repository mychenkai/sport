package com.sport.user.entity.po;

import com.sport.user.vo.UserTokenVo;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * 用户的token信息
 */
@Entity
@Table(name = "user_token")
public class UserToken implements java.io.Serializable {
	/**
	 *  记录ID
	 */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	/**
	 * 用户id
	 */
	private Long userId;

	/**
	 *  用户名
	 */
    private String username;

	/**
	 *  登录IP
	 */
 	private String ip;

	/**
	 *  令牌
	 */
 	private String token;


	/**
	 *  过期时间
	 */
 	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
 	private Date expirationTime;

	/**
	 * 状态（normal/kicked/notlogged）
	 */
 	private String status;


 	public UserToken() {

 	}

 	public UserToken(Long userId, String username, String ip, String token, Date expirationTime, String status) {
 		this.expirationTime = expirationTime;
 		this.ip = ip;
 		this.status = status;
 		this.token = token;
 		this.username = username;
 		this.userId = userId;
 	}
 	
 	public UserTokenVo toVo() {
 		UserTokenVo userTokenVo = new UserTokenVo();
		BeanUtils.copyProperties(this,userTokenVo);
 		return userTokenVo;
 	}
 	
 	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
 		return username;
 	}

 	public void setUsername(String username) {
 		this.username = username;
 	}

 	public String getIp() {
 		return ip;
 	}

 	public void setIp(String ip) {
 		this.ip = ip;
 	}

 	public String getToken() {
 		return token;
 	}

 	public void setToken(String token) {
 		this.token = token;
 	}

 	public Date getExpirationTime() {
 		return expirationTime;
 	}

 	public void setExpirationTime(Date expirationTime) {
 		this.expirationTime = expirationTime;
 	}

 	public String getStatus() {
 		return status;
 	}

 	public void setStatus(String status) {
 		this.status = status;
 	}
}
