package com.sport.user.service;

import com.sport.common.CheckUtil;
import com.sport.common.util.SqlUtil;
import com.sport.jpa.parameter.Operator;
import com.sport.jpa.parameter.PredicateBuilder;
import com.sport.page.common.Paging;
import com.sport.user.entity.po.User;
import com.sport.user.filter.UserFilter;
import com.sport.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <pre>项目名称：
 * 接口名称：UserService
 * 接口描述：
 * 创建人：陈凯 Mr_Chen__K@163.com
 * 创建时间：2019/5/23 10:05
 * 修改人：陈凯 Mr_Chen__K@163.com
 * 修改时间：2019/5/23 10:05
 * 修改备注：
 * @version </pre>
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public Object findListPage(UserFilter userFilter) {
        StringBuilder sql = new StringBuilder();
        sql.append(" select * from  user u ");

        if (!CheckUtil.isBlank(userFilter.getUsername())) {
            sql.append(" AND u.username like '%" + SqlUtil.trimSingleQuotes(userFilter.getUsername	()) + "%' ");
        }

        int page = userFilter.getPage().intValue();
        if(page > 0)
            page--;
        if(page < 0)
            page = 0;
        Pageable pageable;
        if(userFilter.isAllData()) {
            pageable = new PageRequest(0, Integer.MAX_VALUE);
        } else {
            pageable = new PageRequest(page, userFilter.getLimit().intValue());
        }
        Page<Map<String,Object>> mapPage =userRepository.nativeQuery4Map(sql.toString(), pageable);
        Paging paging = new Paging(userFilter);
        paging.setTotal(mapPage.getTotalElements());
        paging.setPages((long)mapPage.getTotalPages());
        paging.setRows(mapPage.getContent());
        return paging;
    }

    public User findByName(String userName) {
        return userRepository.findByUserName(userName);
    }

    public void updateUser(User user) {
        userRepository.saveAndFlush(user);
    }
}
