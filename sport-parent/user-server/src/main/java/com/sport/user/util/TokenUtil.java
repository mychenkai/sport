package com.sport.user.util;

import com.sport.common.UserTokenUtil;
import com.sport.user.entity.po.UserToken;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Date;

public class TokenUtil {



	public static UserToken createUserToken(Long userId, String username, String ip, String sessionId) {
		System.out.println("**************createToken ip :" + ip);
		Date now = new Date();
		String appKeyMasterSecret = userId + username + ":" + now.getTime() + sessionId;
		String encodeStr = DigestUtils.md5Hex(appKeyMasterSecret);
	//	String encodeString = Base64Utils.encodeToString(encodeStr.getBytes());
		UserToken userToken = new UserToken(userId, username, ip, encodeStr, DateUtils.addMinutes(now,UserTokenUtil.termOfValidity), "normal");
		return userToken;
	}
}
