package com.sport.user.entity.po;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.persistence.*;

import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import com.sport.common.MapUtil;

/**
 * <pre>项目名称：
 * 接口名称：User
 * 接口描述：
 * 创建人：陈凯 Mr_Chen__K@163.com
 * 创建时间：2019/5/23 10:06
 * 修改人：陈凯 Mr_Chen__K@163.com
 * 修改时间：2019/5/23 10:06
 * 修改备注：
 * @version </pre>
 */
/**
 * 用户账号表
 */
@Entity
@Table(name = "user")
public class User implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -8762349365439452261L;

    private Long id;
    private String userName;
    private String userPwd;
    private String salt;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    private Date loginTime;//登陆时间
    private Date lastLoginTime;//最后登陆时间
    private Integer loginnum;//登录次数
    private Integer errorNum;//错误次数
    @DateTimeFormat(pattern=("yyyy-MM-dd HH:mm:ss"))
    private Date errorTime;//错误时间
    private Integer status;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="userName", columnDefinition="varchar")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    @Column(name="userPwd", columnDefinition="varchar")
    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }
    @Column(name="salt", columnDefinition="varchar")
    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
    @Column(name="createTime", columnDefinition="Date")
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    @Column(name="loginTime", columnDefinition="Date")
    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }
    @Column(name="lastLoginTime", columnDefinition="Date")
    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }
    @Column(name="loginnum", columnDefinition="int")
    public Integer getLoginnum() {
        return loginnum;
    }

    public void setLoginnum(Integer loginnum) {
        this.loginnum = loginnum;
    }
    @Column(name="errorNum", columnDefinition="int")
    public Integer getErrorNum() {
        return errorNum;
    }

    public void setErrorNum(Integer errorNum) {
        this.errorNum = errorNum;
    }
    @Column(name="errorTime", columnDefinition="Date")
    public Date getErrorTime() {
        return errorTime;
    }

    public void setErrorTime(Date errorTime) {
        this.errorTime = errorTime;
    }
    @Column(name="status", columnDefinition="int")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}

