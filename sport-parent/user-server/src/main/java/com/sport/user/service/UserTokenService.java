package com.sport.user.service;

import com.google.gson.Gson;
import com.sport.common.UserTokenUtil;
import com.sport.redis.RedisUtil;
import com.sport.user.vo.UserTokenVo;
import com.sport.user.entity.po.UserToken;
import com.sport.user.entity.redis.UserTokenRedis;
import com.sport.user.repository.UserTokenRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


@Service
public class UserTokenService {
	
    @Autowired
    private UserTokenRepository userTokenRepository;
    
    @Autowired
    private UserTokenRedis userTokenRedis;

   
    @Transactional
    public UserToken create(UserToken userToken) {
    	UserToken newUserToken = userTokenRepository.save(userToken);
        if(newUserToken != null) {
            RedisUtil.setPool(UserTokenUtil.USER_TOKEN_LIST_KEY+newUserToken.getToken(),newUserToken.getToken());
            RedisUtil.exits(UserTokenUtil.USER_TOKEN_LIST_KEY+newUserToken.getToken(),UserTokenUtil.redisTermOfValidity);
            RedisUtil.setPool(newUserToken.getToken(), new Gson().toJson(newUserToken));
            RedisUtil.exits(newUserToken.getToken(),UserTokenUtil.redisTermOfValidity);
        }
        return newUserToken;
    }
    
    @Transactional
    public UserToken update(UserToken userToken) {
        if(userToken != null) {
        	userTokenRedis.update(userToken);
        }
        return userTokenRepository.save(userToken);
    }
    
    @Transactional(readOnly = true)
    public UserTokenVo getUserTokenByToken(String token) {
    	UserToken userToken = new UserToken();
    	String token1 = RedisUtil.getPool(token);
    	userToken = new Gson().fromJson(token1, UserToken.class);
    	if (userToken == null) {
    		List<UserToken> userTokens = userTokenRepository.findByToken(token);
    		userToken = userTokens.size() > 0 ? userTokens.get(0) : null;
    	}
    	return userToken == null ? null : userToken.toVo();
    }

    @Transactional
    public void deleteByToken(String token) {
    	userTokenRedis.deleteByToken(token);
    	userTokenRepository.deleteByToken(token);
    }
    @Transactional(readOnly = true)
    public UserToken getUserTokenByUserId(Long userId) {
        UserToken userToken = userTokenRepository.findByUserId(userId);
        return userToken;
    }

    @Transactional
    public void delByTime(Date date) {
        userTokenRepository.deleteByExpirationTimeBefore(date);
    }
}
