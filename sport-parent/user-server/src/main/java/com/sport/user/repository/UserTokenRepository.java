package com.sport.user.repository;

import com.sport.jpa.repository.ExpandJpaRepository;
import com.sport.user.entity.po.User;
import com.sport.user.entity.po.UserToken;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface UserTokenRepository extends ExpandJpaRepository<UserToken, Long> {
	
    @Query("select u from UserToken u where u.username =?1 and u.ip =?2")
    User findByNameAndEmail(String username, String ip);

    @Query("select u from UserToken u where u.username like :username")
    Page<UserToken> findByUsername(@Param("username") String username, Pageable pageRequest);
    
    @Modifying
    @Query("delete from UserToken u where u.token=?1 ")
    int deleteByToken(String token);
    
    List<UserToken> findByToken(String token);

    /**
     * 查询用的token 信息
     * @param userId
     * @return
     */
    @Query(value = "SELECT * FROM user_token  WHERE user_id=?1 AND (!ISNULL(circle_token) AND circle_token!='') ORDER BY expiration_time DESC LIMIT 1",nativeQuery = true)
    UserToken findByUserId(Long userId);

    void deleteByExpirationTimeBefore(Date date);
}
