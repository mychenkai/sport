package com.sport.user.entity.redis;

import com.sport.redis.BaseRedis;
import com.sport.user.entity.po.UserToken;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;


@Repository
public class UserTokenRedis extends BaseRedis<UserToken> {

	public static final String USER_TOKEN_LIST_KEY = "user_tokens";

	public static final String USER_NAME_TOKEN_SET_KEY = "user_name_user_tokens_";

	public void add(UserToken userToken) {
		Gson gson = new Gson();
		this.redisTemplate.opsForHash().put(USER_TOKEN_LIST_KEY, userToken.getToken(), gson.toJson(userToken));
		this.redisTemplate.opsForSet().add(USER_NAME_TOKEN_SET_KEY + userToken.getUsername(), userToken.getToken());
	}

	public void update(UserToken userToken) {
		Gson gson = new Gson();
		this.redisTemplate.opsForHash().delete(USER_TOKEN_LIST_KEY, userToken.getToken());
		this.redisTemplate.opsForHash().put(USER_TOKEN_LIST_KEY, userToken.getToken(), gson.toJson(userToken));
	}

	public UserToken getUserTokenByToken(String token) {
		Gson gson = new Gson();
		Object object = this.redisTemplate.opsForHash().get(USER_TOKEN_LIST_KEY, token);
		return object == null ? null : gson.fromJson(object.toString(), entityClass);
	}

	public void deleteByToken(String token) {
		UserToken userToken = getUserTokenByToken(token);
		if(userToken != null) {
			this.redisTemplate.opsForSet().remove(USER_NAME_TOKEN_SET_KEY + userToken.getUsername(), token);
			this.redisTemplate.opsForHash().delete(USER_TOKEN_LIST_KEY, token);
		}
	}

}
