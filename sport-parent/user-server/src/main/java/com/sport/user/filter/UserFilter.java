package com.sport.user.filter;


import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sport.common.DateJsonDeserializer;
import com.sport.page.common.PageCommonFilter;

public class UserFilter extends PageCommonFilter {
	
    private Long id;
    
    private String username;
    
    private String mobile;
    
    private String email;
    
    @JsonDeserialize(using = DateJsonDeserializer.class)
    private Date createDate;

    @JsonDeserialize(using = DateJsonDeserializer.class)
    private Date createDateStart;
    
    @JsonDeserialize(using = DateJsonDeserializer.class)
    private Date createDateEnd;


    private String startTime;

    private String endTime;

	/**
	 * 用户姓名
	 */
	private String fullName;

    public UserFilter() {
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getCreateDateStart() {
		return createDateStart;
	}

	public void setCreateDateStart(Date createDateStart) {
		this.createDateStart = createDateStart;
	}

	public Date getCreateDateEnd() {
		return createDateEnd;
	}

	public void setCreateDateEnd(Date createDateEnd) {
		this.createDateEnd = createDateEnd;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
}
