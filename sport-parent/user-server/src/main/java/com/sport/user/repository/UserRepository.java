package com.sport.user.repository;

import com.sport.jpa.repository.ExpandJpaRepository;
import com.sport.user.entity.po.User;
import org.springframework.stereotype.Repository;

/**
 * <pre>项目名称：
 * 接口名称：UserRepository
 * 接口描述：
 * 创建人：陈凯 Mr_Chen__K@163.com
 * 创建时间：2019/5/23 10:16
 * 修改人：陈凯 Mr_Chen__K@163.com
 * 修改时间：2019/5/23 10:16
 * 修改备注：
 * @version </pre>
 */
@Repository
public interface UserRepository extends ExpandJpaRepository<User,Long>{

    User findByUserName(String userName);
}
