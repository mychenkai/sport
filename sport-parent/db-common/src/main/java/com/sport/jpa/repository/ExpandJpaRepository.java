package com.sport.jpa.repository;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.sport.jpa.parameter.Operator;
import com.sport.jpa.parameter.Predicate;
import com.sport.page.common.PageCommonFilter;

@NoRepositoryBean
public interface ExpandJpaRepository<T, ID extends Serializable> extends JpaRepository<T,ID> {

    T findOne(String condition, Object... objects);

    List<T> findAll(String condition, Object... objects);

    List<T> findAll(Iterable<Predicate> predicates, Operator operator);

    List<T> findAll(Iterable<Predicate> predicates, Operator operator, Sort sort);
    
    List<T> findAll(Iterable<Predicate> predicates, Operator operator, Sort sort, PageCommonFilter pageCommonFilter);

    Page<T> findAll(Iterable<Predicate> predicates, Operator operator, Pageable pageable);
    
    public <X> List<X> find(String hql, Map<String, ?> values);

    long count(Iterable<Predicate> predicates, Operator operator);

    List<T> findAll(String condition, Sort sort, Object... objects);

    Page<T> findAll(String condition, Pageable pageable, Object... objects);

    long count(String condition, Object... objects);

    void deleteByIds(Iterable<ID> ids);

    Class<T> getEntityClass();

    List<Map<String,Object>> nativeQuery4Map(String sql);
    
    List<T> nativeQuery4Bean(String sql, Map<String, Object> params, Class<T> class1);

    Page<Map<String,Object>> nativeQuery4Map(String sql, Pageable pageable);

    Map<String,Object> nativeQuery4OneMap(String sql);

    Object nativeQuery4Object(String sql);
    
    int nativeExecute(String sql);
    
    Object nativeQuery4Object(String sql, Map<String, ?> values);
    
    List<Map<String, Object>> nativeQuery4Map(String sql, Map<String, Object> params, int startNum, int pageSize);
    
    List<Map<String, Object>> nativeQuery4Map(String sql, Map<String, Object> params);
    
    public <X> List<X> findWithSplitPage(String hql, Map<String, ?> values, int startNum, int pageSize);
    
    List<Object[]> nativeQuery4ObjectArray(String sql, Map<String, Object> params);
    
    List<String> nativeQuery4List(String sql, Map<String, Object> params);
    
    public Long count(String hql, Map<String, ?> params);
    
    void detect(T entity);
}
