package com.sport.user.facade;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sport.common.JsonResult;
import com.sport.user.filter.UserFilter;
import com.sport.user.vo.UserTokenVo;
import com.sport.user.vo.UserVo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@RequestMapping("/user")
public interface UserFacade {

    /**
     * 用户登录
     * @param
     * @return 登录结果,
     *            成功：包含token信息
     *            失败：包含失败原因
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public JsonResult login(@RequestBody UserVo userVo);

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public JsonResult list(UserFilter userFilter);

    @RequestMapping(value = "/exportExcel", method = RequestMethod.GET)
    public void exportExcel(HttpServletResponse response) throws Exception;
    /**
     * token验证
     * @param token, 包含登录信息的token
     * @return 验证结果
     */
    @RequestMapping(value = "/validateToken", method = RequestMethod.GET)
    public Boolean validateToken(@RequestParam("token") String token);

    /**
     * 获取userTokenVo
     * @param token, 包含登录信息的token
     * @return userTokenVo
     */
    @RequestMapping(value = "/getUserToken", method = RequestMethod.GET)
    public UserTokenVo getUserToken(@RequestParam("token") String token);
}