package com.sport.user.vo;

import com.alibaba.fastjson.JSONObject;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class UserTokenVo {

	/**
	 *  记录ID
	 */
	private Long id;
	
	/**
	 * 用户id
	 */
	private Long userId;

	/**
	 *  用户名
	 */
	private String username;

	/**
	 *  登录IP
	 */
	private String ip;

	/**
	 *  令牌
	 */
	private String token;

	/**
	 * 登录方式(pc/app)
	 */
	private String loginWith;

	/**
	 * 过期时间
	 */
	private Date expirationTime;

	/**
	 * 状态（normal/kicked/notlogged）
	 */
	private String status;

	/**
	 * jvm缓存本地过期时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date localExpirationTime;

	/**
	 * 飞手圈的token
	 */
	private String circleToken;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getLoginWith() {
		return loginWith;
	}

	public void setLoginWith(String loginWith) {
		this.loginWith = loginWith;
	}

	public Date getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLocalExpirationTime() {
		return localExpirationTime;
	}

	public void setLocalExpirationTime(Date localExpirationTime) {
		this.localExpirationTime = localExpirationTime;
	}

	public String getCircleToken() {
		return circleToken;
	}

	public void setCircleToken(String circleToken) {
		this.circleToken = circleToken;
	}

	public String toJSON(){
		return JSONObject.toJSONString(this);
	}
}
