package com.sport.common;

import com.sport.redis.RedisUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

/**
 * <pre>项目名称：
 * 接口名称：CodeUtil
 * 接口描述：
 * 创建人：陈凯 Mr_Chen__K@163.com
 * 创建时间：2019/8/16 16:05
 * 修改人：陈凯 Mr_Chen__K@163.com
 * 修改时间：2019/8/16 16:05
 * 修改备注：
 * @version </pre>
 */
@RestController
@RequestMapping("/code")
public class CodeUtil extends HttpServlet {

    @RequestMapping(value = "/getCode", method = RequestMethod.GET)
    public JsonResult getCode(HttpServletRequest request,HttpServletResponse response){
        JsonResult jsonResult = new JsonResult(JsonResult.ERROR_STATUS,JsonResult.ERROR_MESSAGE);
        String scode = "";
        Random r = new Random();
        for (int i = 0; i <4; i++) {
            String rand = String.valueOf(r.nextInt(10));
            scode += rand;
        }
        //根据指定的cookie的name来获取值
//        String fhIdInfo = CookieUtil.readCookie(request, "test1");
////        if (StringUtils.isEmpty(fhIdInfo)){
////            //生成随机唯一的值
////            fhIdInfo = UUID.randomUUID().toString().replace("-", "").toUpperCase();
////            //
////            CookieUtil.writeCookie("test1",fhIdInfo,"ck-server",-1,response);
////        }
        String fhIdInfo = UUID.randomUUID().toString().replace("-", "").toUpperCase();
        //开辟空间将验证码存放到redis中
        RedisUtil.setPool("code:"+fhIdInfo,scode);
        //设置过期时间
        RedisUtil.exits("code:"+fhIdInfo,1*60);
        Map<String,Object> map = new HashMap<>();
        map.put("code",scode);
        map.put("cookie",fhIdInfo);
        jsonResult.toSuccess();
        jsonResult.setData(map);
        return jsonResult;
    }
}
