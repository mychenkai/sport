package com.sport.user.controller;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.sport.common.*;
import com.sport.user.entity.po.UserToken;
import com.sport.user.util.TokenUtil;
import com.sport.user.vo.UserTokenVo;
import com.sport.log.SystemLog;
import com.sport.redis.RedisUtil;
import com.sport.user.entity.po.User;
import com.sport.user.facade.UserFacade;
import com.sport.user.filter.UserFilter;
import com.sport.user.service.UserService;
import com.sport.user.service.UserTokenService;
import com.sport.user.vo.UserVo;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.springframework.web.context.request.RequestContextHolder;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.*;


@RestController
public class UserController extends BaseController implements UserFacade {

    private static Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserTokenService userTokenService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private HttpServletRequest request;

    private ListOperations<String, String> opsForList;

    @SystemLog("查询用户")
    public JsonResult list(UserFilter userFilter) {
        JsonResult jsonResult = new JsonResult(JsonResult.SUCCESS_STATUS, JsonResult.SUCCESS_MESSAGE);
        Long userId = getCurrentUserId();
        if(userId == null || userId == 0) {
            return new JsonResult(JsonResult.ERROR_STATUS, "无效的用户信息！");
        };
        jsonResult.setData(userService.findListPage(userFilter));
        Map map = new HashMap();
        map.put("测试","定时任务");
        String str = JSON.toJSONString(map);
        //存放数据到redis
        redisTemplate.opsForList().leftPush("test",str);
        System.out.println("调用接口");
//        MappingJacksonValue mappingJacksonValue = new MappingJacksonValue(jsonResult);
//        mappingJacksonValue.setJsonpFunction(callback);
//        return  mappingJacksonValue;
        return jsonResult;
    }
    @Scheduled(fixedDelay = 5000)
    @Async
    @SystemLog("定时删除用户token")
    public void delUserToken(){
        Date date = new Date();
        try {
            userTokenService.delByTime(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("======定时删除用户token成功=======");
    }

    @Scheduled(fixedDelay = 5000)
    @Async
    @SystemLog("测试定时任务")
    public void test(){
        try {
            Thread.sleep(5000);
            logger.info("<---睡眠5秒--->" );
            Map map = new HashMap();
            map.put("测试","定时任务");
            String str = JSON.toJSONString(map);
            RedisUtil.setPool("test1",str);
            String s = RedisUtil.getPool("test1");
            logger.info("值为---->"+ s );
            //从redis中获取指定缓存数据
            RedisSerializer<String> stringSerializer = new StringRedisSerializer();
            redisTemplate.setKeySerializer(stringSerializer );
            redisTemplate.setValueSerializer(stringSerializer );
            redisTemplate.setHashKeySerializer(stringSerializer );
            redisTemplate.setHashValueSerializer(stringSerializer );
            opsForList = redisTemplate.opsForList();
            String value = opsForList.rightPop("test");
            logger.info("值为---->"+ value );
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("测试定时任务");
    }

    @SystemLog("下载excel")
    public void exportExcel(HttpServletResponse response) throws Exception {
        //查询要导出的数据
        //List<User> userList = (List<User>) userService.findListPage(new UserFilter());
        List<User> userList = new ArrayList<>();
        User user = new User();
        user.setUserName("测试");
        user.setId(1l);
        user.setCreateTime(new Date());
        userList.add(user);
        //导出excel
        HSSFWorkbook workbook = new HSSFWorkbook();
        //XSSFWorkbook workbook = new XSSFWorkbook();//新建工作簿
        HSSFSheet sheet = workbook.createSheet("员工表");//新建表头
        //构建标题
        buildTitle(workbook, sheet);
        //构建表头
        buildHeader(workbook, sheet);
        //构建主题
        bulidBody(userList, workbook, sheet);
        //下载excel
        FileUtil.excelDownload(workbook,response);
    }
    private void buildTitle(HSSFWorkbook workbook, HSSFSheet sheet) {
        HSSFRow title = sheet.createRow(4);				//新建标题
        HSSFCellStyle style = workbook.createCellStyle();//新建标题样式
        HSSFCell titleCell = title.createCell(6);		//新建列
        titleCell.setCellValue("员工信息");				//赋值
        sheet.addMergedRegion(new CellRangeAddress(4, 4, 6, 10));//合并行列,依此是开始行号,结束行号,开始列号，结束列号
        style.setAlignment(XSSFCellStyle.ALIGN_CENTER);		//水平居中对齐
        HSSFFont font = workbook.createFont();				//添加字体
        font.setFontHeightInPoints((short)28);				//设置字号
        style.setFont(font);								//向样式中添加字体
        style.setFillForegroundColor(HSSFColor.RED.index);	//设置图案颜色
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);//设置图案样式
        titleCell.setCellStyle(style);
    }

    private void buildHeader(HSSFWorkbook workbook, HSSFSheet sheet) throws Exception {
        HSSFRow header = sheet.createRow(5);                //向标头下添加一行
        HSSFCellStyle headerStyle = workbook.createCellStyle();//新建样式
        headerStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);//水平居中对齐
        HSSFFont headerFont = workbook.createFont();        //添加字体
        headerFont.setFontHeightInPoints((short) 16);        //设置字号
        headerStyle.setFont(headerFont);                    //向样式中添加字体
        headerStyle.setFillForegroundColor(HSSFColor.PINK.index);//设置图案颜色
        headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);//设置图案样式
        String[] titleArr = {"员工姓名", "员工性别", "员工生日", "员工薪资", "所属部门"};
        int start = 6;
        int end = titleArr.length + 6;
        for (int i = start; i < end; i++) {
            HSSFCell headerCell = header.createCell(i);        //添加一列
             headerCell.setCellValue(titleArr[i-start]);		//赋值
            BufferedImage bufferImg1 = null;//图片
            //判断图片是否存在
            String baseImage = "C:\\Users\\kai\\Desktop\\photos\\2.jpg";
            ByteArrayOutputStream byteArrayOut1 = new ByteArrayOutputStream();
            HSSFPatriarch patriarch = sheet.createDrawingPatriarch();
            if (new File(baseImage).exists()) {
                bufferImg1 = ImageIO.read(new File(baseImage));
//FileUtils.getFileExtension获取图片的后缀名，是jpg还是png
                ImageIO.write(bufferImg1,"jpg", byteArrayOut1);
                //图片一导出到单元格B6中
                HSSFClientAnchor anchor1 = new HSSFClientAnchor(0, 0, 1023, 0,
                        (short) 0, 0, (short) 3, 8);
                patriarch.createPicture(anchor1, workbook.addPicture(byteArrayOut1
                        .toByteArray(), HSSFWorkbook.PICTURE_TYPE_JPEG));
            }
                headerCell.setCellStyle(headerStyle);            //添加样式
            }
        }

        private  byte[] readInputStream(InputStream inStream) throws Exception{
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            //创建一个Buffer字符串
            byte[] buffer = new byte[1024];
            //每次读取的字符串长度，如果为-1，代表全部读取完毕
            int len = 0;
            //使用一个输入流从buffer里把数据读取出来
            while( (len=inStream.read(buffer)) != -1 ){
                //用输出流往buffer里写入数据，中间参数代表从哪个位置开始读，len代表读取的长度
                outStream.write(buffer, 0, len);
            }
            //关闭输入流
            inStream.close();
            //把outStream里的数据写入内存
            return outStream.toByteArray();
        }
    private  void bulidBody(List<User> userList, HSSFWorkbook workbook, HSSFSheet sheet) {
        HSSFCellStyle dateDellStyle = workbook.createCellStyle();//添加样式
        dateDellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy"));//设置日期格式
        for (int i = 0; i < userList.size(); i++) {
            User user = userList.get(i);
            HSSFRow row = sheet.createRow(i+6);
            row.createCell(6).setCellValue(user.getUserName());
            row.createCell(7).setCellValue((user.getId()==1?"男":"女"));
            row.createCell(7).setCellValue(String.valueOf(user.getId()));
            row.createCell(10).setCellValue(user.getId());
            HSSFCell timeCell = row.createCell(8);
            timeCell.setCellValue(user.getCreateTime());
            timeCell.setCellStyle(dateDellStyle);
        }
    }
    public JsonResult login(UserVo userVo){
        JsonResult jsonResult = new JsonResult(JsonResult.ERROR_STATUS,JsonResult.ERROR_MESSAGE);
        //验证登陆信息是否完整
        if(!StringUtils.isNotBlank(userVo.getUserName())||!StringUtils.isNotBlank(userVo.getUserPwd())||!StringUtils.isNotBlank(userVo.getCode())){
            return jsonResult.error(SystemEnum.LOGIN_INFO_ERROR.getMessage());
        }
        //根据name获得cookie的value
        //String sportId = CookieUtil.readCookie(request, "sport");
        String sportId = userVo.getCookie();
        //不存在的话认为是非法登陆
        if (StringUtils.isEmpty(sportId)){
            return jsonResult.error(SystemEnum.LOGIN_FAIL_ERROR.getMessage());
        }
        //根据cookie的值从redis中获得验证码
        String code = RedisUtil.getPool("code:"+sportId);
        //验证码时间超时
        if (StringUtils.isEmpty(code)){
            return jsonResult.error(SystemEnum.CODE_TIMEOUT.getMessage());
        }
        //和用户输入的验证码进行对比
        if(!userVo.getCode().equals(code)){
            return jsonResult.error(SystemEnum.SMS_CODE_ERROR.getMessage());
        }
        //根据用户名查询员工信息
        User user = new User();
        try {
            user=userService.findByName(userVo.getUserName());
        }catch (Exception e){
            return jsonResult.error(SystemEnum.FIND_EMPINFO_ERROR.getMessage());
        }
//        //转换时间格式
//        if (null != user && null != user.getLastLoginTime()){
//            user.getLastLoginTime(DateUtil.convertDateToStr(user.getLastLoginTime(),DateUtil.Y_M_D_H_m_s));
//        }
        //用户信息为空的话返回用户不存在的提示
        if(null==user){
            return jsonResult.error(SystemEnum.LOGIN_USERNAME_NOT_EXITS.getMessage());
        }
        //判断用户是否位锁定状态
        if(user.getStatus()==SystemConst.USER_LOCK){
            return jsonResult.error(SystemEnum.LOGIN_USER_LOCK.getMessage());
        }
        //验证是否是同一天登陆
        if (null!=user.getErrorTime()){
            Date errorTime =DateUtil.convertStrToDate(DateUtil.convertDateToStr(user.getErrorTime(),DateUtil.Y_M_D),DateUtil.Y_M_D);
            Date date = DateUtil.convertStrToDate(DateUtil.convertDateToStr(new Date(),DateUtil.Y_M_D),DateUtil.Y_M_D);
            if (date.after(errorTime)){
                //不是同一天的话，错误次数设为0
                user.setErrorNum(0);
                userService.updateUser(user);
            }
        }
        //验证密码是否正确
        if(!Md5Util.md5(userVo.getUserPwd()+"_"+user.getSalt()).equals(user.getUserPwd())){
        //if(!userVo.getUserPwd().equals(user.getUserPwd())){
            //判断错误次数是否到达二次
            if (user.getErrorNum()>=SystemConst.ERROR_NUM-1){
                //错误次数到达三次的话，修改错误次数和错误时间同时将用户账号设为锁定
                user.setErrorNum(user.getErrorNum()+1);
                user.setErrorTime(new Date());
                user.setStatus(SystemConst.USER_LOCK);
                userService.updateUser(user);
                return jsonResult.error(SystemEnum.LOGIN_PASSWORD_ERROR_NUM.getMessage());
            }else {
                //密码错误的话，修改错误次数和错误登陆时间
                user.setErrorNum(user.getErrorNum()+1);
                user.setErrorTime(new Date());
                userService.updateUser(user);
                return jsonResult.error(SystemEnum.LOGIN_PASSWORD_ERROR.getMessage());
            }
        }else{
            //密码正确的话就把错误次数设为0
            user.setErrorNum(0);
            userService.updateUser(user);
        }
        //验证最后登陆时间是否为空
        if (user.getLastLoginTime()!=null){
            //不为空的情况
            Date lastLoginTime =DateUtil.convertStrToDate(DateUtil.convertDateToStr(user.getLastLoginTime(),DateUtil.Y_M_D),DateUtil.Y_M_D);
            Date date = DateUtil.convertStrToDate(DateUtil.convertDateToStr(new Date(),DateUtil.Y_M_D),DateUtil.Y_M_D);
            //判断是否是同一天同时更新最后登陆时间
            if (date.after(lastLoginTime)){
                //不是同一天的话就把登陆次数设为1
                user.setLoginnum(1);
                user.setLastLoginTime(new Date());
                userService.updateUser(user);
            }else{
                //是同一天的话就把登陆次数加1同时更新最后登陆时间
                user.setLoginnum(user.getLoginnum()+1);
                user.setLastLoginTime(new Date());
                userService.updateUser(user);
            }
        }else{
            //为空的情况
            //登陆次数设为1同时更新最后登陆时间
            user.setLoginnum(1);
            user.setLastLoginTime(new Date());
            userService.updateUser(user);
        }

        //修改初次登陆时间，最后登陆时间，登陆次数,修改错误次数
        user.setErrorNum(0);
        user.setStatus(0);
        userService.updateUser(user);
        //登陆验证通过生成token
        if (user != null){
            UserToken userToken = TokenUtil.createUserToken(user.getId(), user.getUserName(), IpUtil.getIpAddress(request),RequestContextHolder.getRequestAttributes().getSessionId());
            userToken = userTokenService.create(userToken);
            if (userToken != null)
            {
                Map map = new HashMap();
                map.put("token",userToken.getToken());
                jsonResult.setData(map);
                jsonResult.toSuccess();
            } else {
                return jsonResult.error("生成token失败");
            }
        }
        return jsonResult;
    }
    public Boolean validateToken(@RequestParam("token") String token)
    {
        UserTokenVo userTokenVo = userTokenService.getUserTokenByToken(token);
        if (userTokenVo == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    @Override
    public UserTokenVo getUserToken(@RequestParam("token") String token)
    {
        return userTokenService.getUserTokenByToken(token);
    }
    public UserTokenVo test(@RequestParam("token") String token)
    {
        return userTokenService.getUserTokenByToken(token);
    }

}
