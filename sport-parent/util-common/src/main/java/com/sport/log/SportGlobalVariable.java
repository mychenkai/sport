package com.sport.log;

public class SportGlobalVariable {
	
	private static SportGlobalVariable _inst = new SportGlobalVariable();
	
	private SportGlobalVariable(){
		
	}
	
	public static SportGlobalVariable getInstance(){
		return _inst;
	}
	
	private boolean isLogAccessFilter = false;
	
	private boolean isLogAopDebug = false;

	public boolean isLogAccessFilter() {
		return isLogAccessFilter;
	}

	public void setLogAccessFilter(boolean isLogAccessFilter) {
		this.isLogAccessFilter = isLogAccessFilter;
	}

	public boolean isLogAopDebug() {
		return isLogAopDebug;
	}

	public void setLogAopDebug(boolean isLogAopDebug) {
		this.isLogAopDebug = isLogAopDebug;
	}
	
}
