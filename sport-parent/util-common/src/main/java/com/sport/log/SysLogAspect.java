package com.sport.log;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class SysLogAspect
{
    private static final Logger logger = Logger.getLogger(SysLogAspect.class);
    private static final org.slf4j.Logger logger1=LoggerFactory.getLogger(SysLogAspect.class);
    /**
     * 定义Pointcut，Pointcut的名称，此方法不能有返回值，该方法只是一个标示
     */
    // @Pointcut("@annotation(org.springframework.web.bind.annotation.RequestMapping)")
    // @Pointcut("@annotation(org.springframework.web.bind.annotation.RequestMapping) && args(..)")
    @Pointcut("execution(* com.sport.*.controller.*.*(..))")
    public void controllerAspect()
    {
    }

    public Map<String,String> doLog(ProceedingJoinPoint pjp){
        //获得方法签名
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        //获得方法
        Method method = signature.getMethod();
        //判断方法上是否有指定的注解
        String methodname=null;
        if(method.isAnnotationPresent(SystemLog.class)){
            //获得注解
            SystemLog logAnnotation = method.getAnnotation(SystemLog.class);
            //获得注解上的值
            methodname = logAnnotation.value();
        }
        //Log log=new Log();
        //获得类名
        String className = pjp.getTarget().getClass().getCanonicalName();
        //获得方法名
        String methodName = signature.getName();
        //获得request对象
        //HttpServletRequest request = WebContext.getRequest();
        //获得session中的数据
        //Employee userInfo = (Employee) request.getSession().getAttribute("userInfo");
        String userName="";
//        if(null!=userInfo){
//            userName=userInfo.getEmployeeName();
//        }
        //开始时间
        long start=0l;
        //结束时间
        long end=0l;
        Object result=null;
        Map<String,String> resultMap =new HashMap();
        /*try {
            //执行开始的时间
            start = System.currentTimeMillis();
            //执行真正的业务
            //result = pjp.proceed();
            //执行结束时间
            end=System.currentTimeMillis();
            logger1.info("用户{}成功执行了{}类中的{}方法,用了{}毫秒",userName,className,methodname,(end-start));
            //赋值
            String info="成功执行了"+methodname+"方法";
            String msg="用户"+userName+"成功执行了"+className+"类中的"+methodname+"方法,用了"+(end-start)+"毫秒";
            //增加日志到数据库
            //addLogToDB(log, request, userName, start, end,info,msg,1);
        } catch (Throwable e) {
            e.printStackTrace();
            logger1.error("用户{}执行{}类中的{}方法时出现异常,用了{}毫秒",userName,className,methodname,(end-start));
            String info="执行了"+methodname+"方法时出现异常";
            String msg="用户"+userName+"执行"+className+"类中的"+methodname+"方法时出现异常,用了"+(end-start)+"毫秒";
            //增加日志到数据库
            //addLogToDB(log, request, userName, start, end,info,msg,0);
            //return ServerResponse.error();
        }*/
        resultMap.put("userName",userName);
        resultMap.put("className",className);
        resultMap.put("methodname",methodname);
        return resultMap;
    }
    public void writeLog(Map<String,String> resultMap){
        String userName = resultMap.get("userName");
        String className = resultMap.get("className");
        String methodname = resultMap.get("methodname");
        String time = resultMap.get("time");
        try {
            logger1.info("用户{}成功执行了{}类中的{}方法,用了{}毫秒",userName,className,methodname,time);
            //赋值
            String info="成功执行了"+methodname+"方法";
            String msg="用户"+userName+"成功执行了"+className+"类中的"+methodname+"方法,用了"+time+"毫秒";
            //增加日志到数据库
            //addLogToDB(log, request, userName, start, end,info,msg,1);
        } catch (Throwable e) {
            e.printStackTrace();
            logger1.error("用户{}执行{}类中的{}方法时出现异常,用了{}毫秒",userName,className,methodname,time);
            String info="执行了"+methodname+"方法时出现异常";
            String msg="用户"+userName+"执行"+className+"类中的"+methodname+"方法时出现异常,用了"+time+"毫秒";
            //增加日志到数据库
            //addLogToDB(log, request, userName, start, end,info,msg,0);
            //return ServerResponse.error();
        }
    }
    /**
     * 前置通知（Before advice） ：在某连接点（JoinPoint）之前执行的通知，但这个通知不能阻止连接点前的执行。
     * @param joinPoint
     */
    @Before("controllerAspect()")
    public void doBefore(JoinPoint joinPoint)
    {
    	if(!SportGlobalVariable.getInstance().isLogAopDebug())
    		return;
    	logger.debug("=====SysLogAspect前置通知开始=====");
        try{
        	logger.error("doBefore:        " );//+ JSON.toJSONString(joinPoint));
        }catch(Exception ex) {
        	ex.printStackTrace();
        }
    }

    /**
     * 后通知（After advice） ：当某连接点退出的时候执行的通知（不论是正常返回还是异常退出）。
     * @param joinPoint
     */
    @AfterReturning(pointcut = "controllerAspect()")
    public void doAfter(JoinPoint joinPoint)
    {
    	if(!SportGlobalVariable.getInstance().isLogAopDebug())
    		return;
    	logger.debug("=====SysLogAspect后置通知开始=====");
        try{
        	logger.error("doAfter:         " );//+ JSON.toJSONString(joinPoint));
        }catch(Exception ex) {
        }
    }

    /**
     * 抛出异常后通知（After throwing advice） ： 在方法抛出异常退出时执行的通知。
     * @param joinPoint
     * @param e
     */
    @AfterThrowing(value = "controllerAspect()", throwing = "e")
    public void doAfterThrowing(JoinPoint joinPoint, Exception e)
    {
    	if(!SportGlobalVariable.getInstance().isLogAopDebug())
    		return;
    	logger.debug("=====SysLogAspect异常通知开始=====");
        if(joinPoint == null) {
        	logger.error("doAfterThrowing: " + e.getMessage(), e);
        } else {
        	try{
        		//logger.error("doAfterThrowing: " + JSON.toJSONString(joinPoint), e);
        	}catch(Exception ex) {
        		logger.error("doAfterThrowing e: " + e.getMessage(), e);
        	}
        }
    }

    /**
     * 环绕通知（Around advice） ：包围一个连接点的通知，类似Web中Servlet规范中的Filter的doFilter方法。可以在方法的调用前后完成自定义的行为，也可以选择不执行。
     * @param joinPoint
     */
    @Around("controllerAspect()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable
    {
    	if(!SportGlobalVariable.getInstance().isLogAopDebug()) {
            Object result = null;
            Map<String,String> map = doLog(joinPoint);
            //开始时间
            long start=0l;
            //结束时间
            long end=0l;
            try {
                //执行开始的时间
                start = System.currentTimeMillis();
                //执行真正的业务
                result = joinPoint.proceed();
                //执行结束时间
                end=System.currentTimeMillis();
                logger1.info("用户{}成功执行了{}类中的{}方法,用了{}毫秒",map.get("userName"),map.get("className"),map.get("methodname"),(end-start));
            } catch (Throwable throwable) {
                logger1.error("用户{}执行{}类中的{}方法时出现异常,用了{}毫秒",map.get("userName"),map.get("className"),map.get("methodname"),(end-start));
                throwable.printStackTrace();
            }
    		return result;
    	}
    	logger.debug("=====SysLogAspect 环绕通知开始=====");
        //handleLog(joinPoint, null);
        try{
        	ClassMethodInfo cmi = fromJoinPoint(joinPoint);
        	logger.error("[doAround] Invoke:        " + JSON.toJSONString(cmi));
        }catch(Exception ex) {
        	logger.error("[doAround] Invoke :        ", ex);
        }
        Object obj= joinPoint.proceed();
        try{
        	logger.error("[doAround] Return:      " + JSON.toJSONString(obj));
        }catch(Exception ex) {
        	logger.error("[doAround] Return :        ", ex);
        }
        logger.debug("=====SysLogAspect 环绕通知结束=====");
        return  obj;
    }

    
    private static ClassMethodInfo fromJoinPoint(JoinPoint jp) {
    	ClassMethodInfo cmi = new ClassMethodInfo();
    	Signature signature = jp.getSignature();
    	cmi.setClassName(signature.getDeclaringTypeName());
    	cmi.setMethodName(signature.getName());
    	cmi.setModifier(Modifier.toString(signature.getModifiers()));
    	
    	Object[] args = jp.getArgs();
    	for(int i=0; i<args.length; i++) {
    		cmi.getArguments().add(args[i]);
    	}
    	return cmi;
    }
}
