package com.sport.log;

import java.util.ArrayList;
import java.util.List;

public class ClassMethodInfo {

	private String className;

	private String methodName;

	private String modifier; // public private

	private List<Object> arguments = new ArrayList<Object>();

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public List<Object> getArguments() {
		return arguments;
	}

	public void setArguments(List<Object> arguments) {
		this.arguments = arguments;
	}

}
