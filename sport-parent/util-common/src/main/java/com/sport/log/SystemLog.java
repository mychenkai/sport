package com.sport.log;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
 * <pre>项目名称：shop-admin    
 * 类名称：SystemLog    
 * 类描述：    
 * 创建人：陈凯 Mr_Chen__K@163.com  
 * 创建时间：2018年10月18日 下午3:09:12    
 * 修改人：陈凯 Mr_Chen__K@163.com   
 * 修改时间：2018年10月18日 下午3:09:12    
 * 修改备注：       
 * @version </pre>    
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SystemLog {

	String value() default "";
}
