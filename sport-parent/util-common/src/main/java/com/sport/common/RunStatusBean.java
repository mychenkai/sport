package com.sport.common;

public class RunStatusBean {
	private int runStatus;
	
	private String runStatusEn;
	
	private String runStatusText;
	
	public RunStatusBean(int runStatus, String runStatusEn, String runStatusText) {
		this.setRunStatus(runStatus);
		this.setRunStatusEn(runStatusEn);
		this.setRunStatusText(runStatusText);
	}

	public int getRunStatus() {
		return runStatus;
	}

	public void setRunStatus(int runStatus) {
		this.runStatus = runStatus;
	}

	public String getRunStatusEn() {
		return runStatusEn;
	}

	public void setRunStatusEn(String runStatusEn) {
		this.runStatusEn = runStatusEn;
	}

	public String getRunStatusText() {
		return runStatusText;
	}

	public void setRunStatusText(String runStatusText) {
		this.runStatusText = runStatusText;
	}
	
}
