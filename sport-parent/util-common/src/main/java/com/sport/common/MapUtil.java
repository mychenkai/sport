package com.sport.common;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
//--
public class MapUtil {
	public static Object getObject(Map<?, ?> map, Object key) {
		if (map != null) {
			return map.get(key);
		}
		return null;
	}

	public static String getString(Map<?, ?> map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if (answer != null) {
				return answer.toString();
			}
		}
		return null;
	}

	public static Boolean getBoolean(Map<?, ?> map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if (answer != null) {
				if ((answer instanceof Boolean)) {
					return (Boolean) answer;
				}
				if ((answer instanceof String)) {
					return new Boolean((String) answer);
				}
				if ((answer instanceof Number)) {
					Number n = (Number) answer;
					return n.intValue() != 0 ? Boolean.TRUE : Boolean.FALSE;
				}
			}
		}
		return null;
	}

	public static Number getNumber(Map<?, ?> map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if (answer != null) {
				if ((answer instanceof Number)) {
					return (Number) answer;
				}
				if ((answer instanceof String)) {
					try {
						String text = (String) answer;
						return NumberFormat.getInstance().parse(text);
					} catch (ParseException e) {
						logInfo(e);
					}
				}
			}
		}
		return null;
	}

	public static Byte getByte(Map<?, ?> map, Object key) {
		Number answer = getNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Byte)) {
			return (Byte) answer;
		}
		return new Byte(answer.byteValue());
	}

	public static Short getShort(Map<?, ?> map, Object key) {
		Number answer = getNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Short)) {
			return (Short) answer;
		}
		return new Short(answer.shortValue());
	}

	public static Integer getInteger(Map<?, ?> map, Object key) {
		Number answer = getNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Integer)) {
			return (Integer) answer;
		}
		return new Integer(answer.intValue());
	}

	public static Long getLong(Map<?, ?> map, Object key) {
		Number answer = getNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Long)) {
			return (Long) answer;
		}
		return new Long(answer.longValue());
	}

	public static Float getFloat(Map<?, ?> map, Object key) {
		Number answer = getNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Float)) {
			return (Float) answer;
		}
		return new Float(answer.floatValue());
	}

	public static Double getDouble(Map<?, ?> map, Object key) {
		Number answer = getNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Double)) {
			return (Double) answer;
		}
		return new Double(answer.doubleValue());
	}

	public static java.util.Date getDate(Map<?, ?> map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if (answer != null) {
				if ((answer instanceof java.util.Date)) {
					return (java.util.Date) answer;
				}
				if ((answer instanceof java.sql.Date)) {
					java.sql.Date sqlDate = (java.sql.Date) answer;
					Calendar cal = Calendar.getInstance();
					cal.set(1, sqlDate.getYear());
					cal.set(2, sqlDate.getMonth());
					cal.set(5, sqlDate.getDate());
					cal.set(11, sqlDate.getHours());
					cal.set(12, sqlDate.getMinutes());
					cal.set(13, sqlDate.getSeconds());
					return cal.getTime();
				}
				if ((answer instanceof String)) {
					try {
						String text = (String) answer;
						return SimpleDateFormat.getDateInstance().parse(text);
					} catch (ParseException e) {
						logInfo(e);
					}
				}
			}
		}
		return null;
	}

	public static Map<?, ?> getMap(Map<?, ?> map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if ((answer != null) && ((answer instanceof Map))) {
				return (Map) answer;
			}
		}
		return null;
	}

	public static Object getObject(Map<?, ?> map, Object key, Object defaultValue) {
		if (map != null) {
			Object answer = map.get(key);
			if (answer != null) {
				return answer;
			}
		}
		return defaultValue;
	}

	public static String getString(Map<?, ?> map, Object key, String defaultValue) {
		String answer = getString(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Boolean getBoolean(Map<?, ?> map, Object key, Boolean defaultValue) {
		Boolean answer = getBoolean(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Number getNumber(Map<?, ?> map, Object key, Number defaultValue) {
		Number answer = getNumber(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Byte getByte(Map<?, ?> map, Object key, Byte defaultValue) {
		Byte answer = getByte(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Short getShort(Map<?, ?> map, Object key, Short defaultValue) {
		Short answer = getShort(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Integer getInteger(Map<?, ?> map, Object key, Integer defaultValue) {
		Integer answer = getInteger(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Long getLong(Map<?, ?> map, Object key, Long defaultValue) {
		Long answer = getLong(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Float getFloat(Map<?, ?> map, Object key, Float defaultValue) {
		Float answer = getFloat(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Double getDouble(Map<?, ?> map, Object key, Double defaultValue) {
		Double answer = getDouble(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Map<?, ?> getMap(Map<?, ?> map, Object key, Map<?, ?> defaultValue) {
		Map answer = getMap(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static boolean getBooleanValue(Map<?, ?> map, Object key) {
		Boolean booleanObject = getBoolean(map, key);
		if (booleanObject == null) {
			return false;
		}
		return booleanObject.booleanValue();
	}

	public static byte getByteValue(Map<?, ?> map, Object key) {
		Byte byteObject = getByte(map, key);
		if (byteObject == null) {
			return 0;
		}
		return byteObject.byteValue();
	}

	public static short getShortValue(Map<?, ?> map, Object key) {
		Short shortObject = getShort(map, key);
		if (shortObject == null) {
			return 0;
		}
		return shortObject.shortValue();
	}

	public static int getIntValue(Map<?, ?> map, Object key) {
		Integer integerObject = getInteger(map, key);
		if (integerObject == null) {
			return 0;
		}
		return integerObject.intValue();
	}

	public static long getLongValue(Map<?, ?> map, Object key) {
		Long longObject = getLong(map, key);
		if (longObject == null) {
			return 0L;
		}
		return longObject.longValue();
	}

	public static float getFloatValue(Map<?, ?> map, Object key) {
		Float floatObject = getFloat(map, key);
		if (floatObject == null) {
			return 0.0F;
		}
		return floatObject.floatValue();
	}

	public static double getDoubleValue(Map<?, ?> map, Object key) {
		Double doubleObject = getDouble(map, key);
		if (doubleObject == null) {
			return 0.0D;
		}
		return doubleObject.doubleValue();
	}

	public static boolean getBooleanValue(Map<?, ?> map, Object key, boolean defaultValue) {
		Boolean booleanObject = getBoolean(map, key);
		if (booleanObject == null) {
			return defaultValue;
		}
		return booleanObject.booleanValue();
	}

	public static byte getByteValue(Map<?, ?> map, Object key, byte defaultValue) {
		Byte byteObject = getByte(map, key);
		if (byteObject == null) {
			return defaultValue;
		}
		return byteObject.byteValue();
	}

	public static short getShortValue(Map<?, ?> map, Object key, short defaultValue) {
		Short shortObject = getShort(map, key);
		if (shortObject == null) {
			return defaultValue;
		}
		return shortObject.shortValue();
	}

	public static int getIntValue(Map<?, ?> map, Object key, int defaultValue) {
		Integer integerObject = getInteger(map, key);
		if (integerObject == null) {
			return defaultValue;
		}
		return integerObject.intValue();
	}

	public static long getLongValue(Map<?, ?> map, Object key, long defaultValue) {
		Long longObject = getLong(map, key);
		if (longObject == null) {
			return defaultValue;
		}
		return longObject.longValue();
	}

	public static float getFloatValue(Map<?, ?> map, Object key, float defaultValue) {
		Float floatObject = getFloat(map, key);
		if (floatObject == null) {
			return defaultValue;
		}
		return floatObject.floatValue();
	}

	public static double getDoubleValue(Map<?, ?> map, Object key, double defaultValue) {
		Double doubleObject = getDouble(map, key);
		if (doubleObject == null) {
			return defaultValue;
		}
		return doubleObject.doubleValue();
	}

	public static Properties toProperties(Map<?, ?> map) {
		Properties answer = new Properties();
		if (map != null) {
			for (Iterator iter = map.entrySet().iterator(); iter.hasNext();) {
				Map.Entry entry = (Map.Entry) iter.next();
				Object key = entry.getKey();
				Object value = entry.getValue();
				answer.put(key, value);
			}
		}
		return answer;
	}

	public static Map<String, Object> toMap(ResourceBundle resourceBundle) {
		Enumeration enumeration = resourceBundle.getKeys();
		Map map = new HashMap();

		while (enumeration.hasMoreElements()) {
			String key = (String) enumeration.nextElement();
			Object value = resourceBundle.getObject(key);
			map.put(key, value);
		}

		return map;
	}

	protected static void logInfo(Exception ex) {
//		System.out.println("INFO: Exception: " + ex);
	}

	public static Map<?, ?> invertMap(Map<?, ?> map) {
		Map out = new HashMap(map.size());
		for (Iterator it = map.entrySet().iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			out.put(entry.getValue(), entry.getKey());
		}
		return out;
	}

	public static void safeAddToMap(Map<Object, Object> map, Object key, Object value) throws NullPointerException {
		if (value == null)
			map.put(key, "");
		else
			map.put(key, value);
	}

	public static Map<Object, Object> putAll(Map<Object, Object> map, Object[] array) {
		map.size();
		if ((array == null) || (array.length == 0)) {
			return map;
		}
		Object obj = array[0];
		if ((obj instanceof Map.Entry))
			for (int i = 0; i < array.length; i++) {
				Map.Entry entry = (Map.Entry) array[i];
				map.put(entry.getKey(), entry.getValue());
			}
		else if ((obj instanceof Object[]))
			for (int i = 0; i < array.length; i++) {
				Object[] sub = (Object[]) array[i];
				if ((sub == null) || (sub.length < 2)) {
					throw new IllegalArgumentException("Invalid array element: " + i);
				}
				map.put(sub[0], sub[1]);
			}
		else {
			for (int i = 0; i < array.length - 1;) {
				map.put(array[(i++)], array[(i++)]);
			}
		}
		return map;
	}

	public static boolean isEmpty(Map<?, ?> map) {
		return (map == null) || (map.isEmpty());
	}

	public static boolean isNotEmpty(Map<?, ?> map) {
		return !isEmpty(map);
	}

	public static String PrintMapToString(Map map) {
		return PrintMapToString(map, 0);
	}
	
	private static String[] PADDING = new String[]{"", "    ", "        ", "            ", "                ", "                        ",
		"                        ", "                            ", "                                ", "                                 ", "                                  ", "                                  ", "                                   "};
	
	public static String PrintMapToString(Map map, int level) {
		if(true)
			return "";
		StringBuilder sb = new StringBuilder();
		if(level > 11)
			sb.append("                    ");
		else
			sb.append(PADDING[level]);
		sb.append("{\r\n");
		int i = 0;
		int nCount = map.keySet().size();
		for(Object objKey : map.keySet()){
			String key = (String)objKey;
			Object value = map.get(key);
			if(level > 11)
				sb.append("                    ");
			else
				sb.append(PADDING[level + 1]);
			sb.append(key).append("=");
			if(value instanceof Map) {
				sb.append("\r\n");
				sb.append(PrintMapToString((Map)value, level + 1));
				if(i<nCount - 1){
					sb.insert(sb.length() - 2, ",");
				}
				continue;
			} else {
				sb.append(value);
			}
			if(i<nCount - 1){
				sb.append(",");
			}
			sb.append("\r\n");
			i++;
		}
		sb.append(PADDING[level]).append("}\r\n");
		return sb.toString();
	}

	public static Object PrintMapToString(List<Map<String, Object>> resultData) {
		StringBuilder sb = new StringBuilder();
		sb.append("[\r\n");
		int nCount = resultData.size();
		for(int i=0; i<nCount; i++)
		{
			sb.append(PrintMapToString(resultData.get(i), 1));
			if(i<nCount - 1){
				sb.insert(sb.length() - 2, ",");
			}
		}
		sb.append("]\r\n");
		return sb.toString();
	}

	public static BigDecimal getBigDecimalValue(Map<String, Object> map, String key) {
		if (map != null) {
			Object answer = map.get(key);
			if (answer != null) {
				if ((answer instanceof BigDecimal)) {
					return (BigDecimal) answer;
				}
				if ((answer instanceof Double)) {
					return BigDecimal.valueOf((Double)answer);
				}
				if ((answer instanceof String)) {
					String text = (String) answer;
					return BigDecimal.valueOf(Double.parseDouble(text));
				}
			}
		}
		return null;
	}
}
