package com.sport.common.util;

import java.util.ArrayList;
import java.util.List;

/**
 * list 集合相关的工具类
 */
public class ListUtil {
    /**
     * 去除List 集合中重复的元素
     * @param list
     * @param <T>
     * @return
     */
    public static <T> List<T> removeDuplicate(List<T> list){
        List<T> listTemp = new ArrayList();
        for(int i=0;i<list.size();i++){
            if(!listTemp.contains(list.get(i))){
                listTemp.add(list.get(i));
            }
        }
        return listTemp;
    }
}
