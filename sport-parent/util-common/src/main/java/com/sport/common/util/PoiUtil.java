package com.sport.common.util;

import com.sport.page.common.PoiSheet;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.*;

/**
 *  Excel 工具类   用于对Excel的导入和导出
 */
public class PoiUtil
{
	/**
	 * 2003版的前缀
	 */
	public static final String SUFFIX_2003 = "xls";

	/**
	 * 2007版的前缀
	 */
	public static final String SUFFIX_2007 = "xlsx";

	/**
	 *  导出Excel方法 重载不需要合并单元格的时候更方便
	 * @param sheetName sheet名称
	 * @param titleName 文档的标题
	 * @param fileName  文档的名称
	 * @param columnNumber 列数
	 * @param columnWidth  列的宽度
	 * @param columnName   列的名称
	 * @param dataList     需要导出的数据 一个String List集合
	 * @param out         输出的流向
	 * @throws Exception  可能存在导出失败异常
	 */
	public  static void exportExcel(String sheetName, String titleName, String fileName, int columnNumber, int[] columnWidth, String[] columnName, String[][] dataList, OutputStream out)
			throws Exception {
		exportExcel(sheetName,titleName,fileName,columnNumber,columnWidth,columnName,dataList,null,out);
	}
	public  static void exportExcel(String sheetName, String titleName, String fileName, int columnNumber, int[] columnWidth, String[] columnName, String[][] dataList, OutputStream out, Map<String, List<String>> combineMap)
			throws Exception {
		exportExcel(sheetName,titleName,fileName,columnNumber,columnWidth,columnName,dataList,combineMap,out);
	}
	public  static void exportExcel1(List<Map<String,String>> list,String sheetName, String titleName, String fileName, int columnNumber, int[] columnWidth, String[] columnName, String[][] dataList, OutputStream out, Map<String, List<String>> combineMap)
			throws Exception {
		exportExcel1(list,sheetName,titleName,fileName,columnNumber,columnWidth,columnName,dataList,combineMap,out);
	}
	/**
	 *  导出Excel方法
	 * @param sheetName sheet名称
	 * @param titleName 文档的标题
	 * @param fileName  文档的名称
	 * @param columnNumber 列数
	 * @param columnWidth  列的宽度
	 * @param columnName   列的名称
	 * @param dataList     需要导出的数据 一个String List
	 * @param combineMap  需要合并的单元格
	 * @param out         输出的流向
	 * @throws Exception  可能存在导出失败异常
	 */
	public  static void exportExcel1(List<Map<String,String>> list,String sheetName, String titleName, String fileName, int columnNumber, int[] columnWidth, String[] columnName, String[][] dataList, Map<String, List<String>> combineMap, OutputStream out)
	        throws Exception
	{
		if (columnNumber == columnWidth.length && columnWidth.length == columnName.length)
		{
			// 第一步，创建一个webbook，对应一个Excel文件
			HSSFWorkbook wb = new HSSFWorkbook();
			// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
			HSSFSheet sheet = wb.createSheet(sheetName);
			// sheet.setDefaultColumnWidth(15); //统一设置列宽

			if (combineMap != null && combineMap.get("combine") != null)
			{
				List<String> clist = combineMap.get("combine");
				if (clist != null && clist.size() > 0)
				{
					for (int i = 0; i < clist.size(); i++)
					{
						String t = clist.get(i);
						String[] ts = t.split(",");
						// 在sheet里增加合并单元格
						CellRangeAddress cra = new CellRangeAddress(Integer.parseInt(ts[0]), Integer.parseInt(ts[1]), Integer.parseInt(ts[2]), Integer.parseInt(ts[3]));
						sheet.addMergedRegion(cra);
					}
				}
			}

			for (int i = 0; i < columnNumber; i++)
			{
				for (int j = 0; j <= i; j++)
				{
					if (i == j)
					{
						sheet.setColumnWidth(i, columnWidth[j] * 256); // 单独设置每列的宽
					}
				}
			}
			// 创建第0行 也就是标题
			HSSFRow row1 = sheet.createRow((int) 0);
			row1.setHeightInPoints(50);// 设备标题的高度
			// 第三步创建标题的单元格样式style2以及字体样式headerFont1
			HSSFCellStyle style2 = wb.createCellStyle();
			style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			style2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
			style2.setFillForegroundColor(HSSFColor.LIGHT_TURQUOISE.index);
			style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			HSSFFont headerFont1 = (HSSFFont) wb.createFont(); // 创建字体样式
			headerFont1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 字体加粗
			headerFont1.setFontName("黑体"); // 设置字体类型
			headerFont1.setFontHeightInPoints((short) 15); // 设置字体大小
			style2.setFont(headerFont1); // 为标题样式设置字体样式

			HSSFCell cell1 = row1.createCell(0);// 创建标题第一列
			if (null != dataList && dataList.length > 0 ){
				sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, columnNumber - 1)); // 合并列标题
			}
			cell1.setCellValue(titleName); // 设置值标题
			cell1.setCellStyle(style2); // 设置标题样式

			// 创建第1行 也就是表头
			HSSFRow row = sheet.createRow((int) 1);
			row.setHeightInPoints(37);// 设置表头高度

			// 第四步，创建表头单元格样式 以及表头的字体样式
			HSSFCellStyle style = wb.createCellStyle();
			style.setWrapText(true);// 设置自动换行
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER); // 创建一个居中格式

			style.setBottomBorderColor(HSSFColor.BLACK.index);
			style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			style.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style.setBorderTop(HSSFCellStyle.BORDER_THIN);

			HSSFFont headerFont = (HSSFFont) wb.createFont(); // 创建字体样式
			headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 字体加粗
			headerFont.setFontName("黑体"); // 设置字体类型
			headerFont.setFontHeightInPoints((short) 10); // 设置字体大小
			style.setFont(headerFont); // 为标题样式设置字体样式

			// 第四.一步，创建表头的列
			for (int i = 0; i < columnNumber; i++)
			{
				HSSFCell cell = row.createCell(i);
				cell.setCellValue(columnName[i]);
				cell.setCellStyle(style);
			}
			//创建个人赛事表
			HSSFSheet sheet1 = wb.createSheet("个人赛信息");
			for (int i = 0; i < 5; i++) {
				sheet1.setColumnWidth(i, 4000); // 单独设置每列的宽
			}
			//创建表头
			HSSFRow row2 = sheet1.createRow(0);
			row2.setHeightInPoints(50);// 设备标题的高度
			HSSFCell cell = row2.createCell(0);
			cell.setCellValue("个人赛事信息表");
			sheet1.addMergedRegion(new CellRangeAddress(0,0,0,4));
			cell.setCellStyle(style2);
			//创建标题
			HSSFRow row3 = sheet1.createRow(1);
			row3.setHeightInPoints(37);// 设置表头高度
			String[] arr = {"序号","赛事名称","赛事类型","参赛人","审核状态"};
			int start = 0;
			int end = arr.length;
			for (int i = start; i < end; i++) {
				HSSFCell cell2 = row3.createCell(i);
				cell2.setCellValue(arr[i-start]);
				cell2.setCellStyle(style);
			}
			if (null != list && list.size() > 0 ){
				// 为数据内容设置特点新单元格样式1 自动换行 上下居中
				HSSFCellStyle zidonghuanhang1 = wb.createCellStyle();
				zidonghuanhang1.setWrapText(true);// 设置自动换行
				zidonghuanhang1.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER); // 创建一个居中格式

				// 设置边框
				zidonghuanhang1.setBottomBorderColor(HSSFColor.BLACK.index);
				zidonghuanhang1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang1.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang1.setBorderRight(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang1.setBorderTop(HSSFCellStyle.BORDER_THIN);

				// 为数据内容设置特点新单元格样式2 自动换行 上下居中左右也居中
				HSSFCellStyle zidonghuanhang3 = wb.createCellStyle();
				zidonghuanhang3.setWrapText(true);// 设置自动换行
				zidonghuanhang3.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER); // 创建一个上下居中格式
				zidonghuanhang3.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 左右居中

				// 设置边框
				zidonghuanhang3.setBottomBorderColor(HSSFColor.BLACK.index);
				zidonghuanhang3.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang3.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang3.setBorderRight(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang3.setBorderTop(HSSFCellStyle.BORDER_THIN);
				int a = 0;
				for (int i = 0; i <list.size() ; i++) {
					HSSFRow row4 = sheet1.createRow(i+2);
					HSSFCell cell3 =row4.createCell(0);
					cell3.setCellValue(++a);
					cell3.setCellStyle(zidonghuanhang3);
					HSSFCell cell4 =row4.createCell(1);
					cell4.setCellValue(list.get(i).get("赛事名称"));
					cell4.setCellStyle(zidonghuanhang3);
					HSSFCell cell5 =row4.createCell(2);
					cell5.setCellValue(list.get(i).get("赛事类型"));
					cell5.setCellStyle(zidonghuanhang3);
					HSSFCell cell6 =row4.createCell(3);
					cell6.setCellValue(list.get(i).get("参赛人"));
					cell6.setCellStyle(zidonghuanhang3);
					HSSFCell cell7 =row4.createCell(4);
					cell7.setCellValue(list.get(i).get("状态"));
					cell7.setCellStyle(zidonghuanhang3);
				}

				HSSFRow row4 = sheet1.createRow(list.size()+2);
				row4.setHeightInPoints(30);
				HSSFCell cell6 =row4.createCell(3);
				cell6.setCellValue("签字（盖章）：");
				cell6.setCellStyle(zidonghuanhang3);
				HSSFRow row5 = sheet1.createRow(list.size()+3);
				row5.setHeightInPoints(30);
				HSSFCell cell7 =row5.createCell(3);
				cell7.setCellValue("报名日期：");
				cell7.setCellStyle(zidonghuanhang3);
			}
			// 第五步，创建单元格，并设置值
			for (int i = 0; dataList != null && i < dataList.length; i++)
			{
				row = sheet.createRow((int) i + 2);
				// 为数据内容设置特点新单元格样式1 自动换行 上下居中
				HSSFCellStyle zidonghuanhang = wb.createCellStyle();
				zidonghuanhang.setWrapText(true);// 设置自动换行
				zidonghuanhang.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER); // 创建一个居中格式

				// 设置边框
				zidonghuanhang.setBottomBorderColor(HSSFColor.BLACK.index);
				zidonghuanhang.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang.setBorderRight(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang.setBorderTop(HSSFCellStyle.BORDER_THIN);

				// 为数据内容设置特点新单元格样式2 自动换行 上下居中左右也居中
				HSSFCellStyle zidonghuanhang2 = wb.createCellStyle();
				zidonghuanhang2.setWrapText(true);// 设置自动换行
				zidonghuanhang2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER); // 创建一个上下居中格式
				zidonghuanhang2.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 左右居中

				// 设置边框
				zidonghuanhang2.setBottomBorderColor(HSSFColor.BLACK.index);
				zidonghuanhang2.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang2.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang2.setBorderRight(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang2.setBorderTop(HSSFCellStyle.BORDER_THIN);
				HSSFCell datacell = null;
				for (int j = 0; j < columnNumber; j++)
				{
					datacell = row.createCell(j);
					datacell.setCellValue(dataList[i][j]);
					datacell.setCellStyle(zidonghuanhang2);
				}
			}

			if (out != null)
			{
				// 第六步，将文件存到浏览器设置的下载位置
				String filename = fileName + ".xls";
				// filename="abc.xls";
				try
				{
					wb.write(out);// 将数据写出去
					String str = "导出" + fileName + "成功！";
					System.out.println(str);
				}
				catch (Exception e)
				{
					e.printStackTrace();
					String str1 = "导出" + fileName + "失败！";
					System.out.println(str1);
				}
				finally
				{
					out.close();
				}
			}
			else
			{
				// 第六步，将文件存到指定位置
				try
				{
					FileOutputStream fout = new FileOutputStream("C:WWWW.xls");
					wb.write(fout);
					String str = "导出" + fileName + "成功！";
					System.out.println(str);
					fout.close();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					String str1 = "导出" + fileName + "失败！";
					System.out.println(str1);
				}
			}

		}
		else
		{
			System.out.println("列数目长度名称三个数组长度要一致");
		}

	}
	public  static void exportExcel(String sheetName, String titleName, String fileName, int columnNumber, int[] columnWidth, String[] columnName, String[][] dataList, Map<String, List<String>> combineMap, OutputStream out)
	        throws Exception
	{
		if (columnNumber == columnWidth.length && columnWidth.length == columnName.length)
		{
			// 第一步，创建一个webbook，对应一个Excel文件
			HSSFWorkbook wb = new HSSFWorkbook();
			// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
			HSSFSheet sheet = wb.createSheet(sheetName);
			// sheet.setDefaultColumnWidth(15); //统一设置列宽

			if (combineMap != null && combineMap.get("combine") != null)
			{
				List<String> clist = combineMap.get("combine");
				if (clist != null && clist.size() > 0)
				{
					for (int i = 0; i < clist.size(); i++)
					{
						String t = clist.get(i);
						String[] ts = t.split(",");
						// 在sheet里增加合并单元格
						CellRangeAddress cra = new CellRangeAddress(Integer.parseInt(ts[0]), Integer.parseInt(ts[1]), Integer.parseInt(ts[2]), Integer.parseInt(ts[3]));
						sheet.addMergedRegion(cra);
					}
				}
			}

			for (int i = 0; i < columnNumber; i++)
			{
				for (int j = 0; j <= i; j++)
				{
					if (i == j)
					{
						sheet.setColumnWidth(i, columnWidth[j] * 256); // 单独设置每列的宽
					}
				}
			}
			// 创建第0行 也就是标题
			HSSFRow row1 = sheet.createRow((int) 0);
			row1.setHeightInPoints(50);// 设备标题的高度
			// 第三步创建标题的单元格样式style2以及字体样式headerFont1
			HSSFCellStyle style2 = wb.createCellStyle();
			style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			style2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
			style2.setFillForegroundColor(HSSFColor.LIGHT_TURQUOISE.index);
			style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			HSSFFont headerFont1 = (HSSFFont) wb.createFont(); // 创建字体样式
			headerFont1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 字体加粗
			headerFont1.setFontName("黑体"); // 设置字体类型
			headerFont1.setFontHeightInPoints((short) 15); // 设置字体大小
			style2.setFont(headerFont1); // 为标题样式设置字体样式

			HSSFCell cell1 = row1.createCell(0);// 创建标题第一列
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, columnNumber - 1)); // 合并列标题
			cell1.setCellValue(titleName); // 设置值标题
			cell1.setCellStyle(style2); // 设置标题样式

			// 创建第1行 也就是表头
			HSSFRow row = sheet.createRow((int) 1);
			row.setHeightInPoints(37);// 设置表头高度

			// 第四步，创建表头单元格样式 以及表头的字体样式
			HSSFCellStyle style = wb.createCellStyle();
			style.setWrapText(true);// 设置自动换行
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER); // 创建一个居中格式

			style.setBottomBorderColor(HSSFColor.BLACK.index);
			style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			style.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style.setBorderTop(HSSFCellStyle.BORDER_THIN);

			HSSFFont headerFont = (HSSFFont) wb.createFont(); // 创建字体样式
			headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 字体加粗
			headerFont.setFontName("黑体"); // 设置字体类型
			headerFont.setFontHeightInPoints((short) 10); // 设置字体大小
			style.setFont(headerFont); // 为标题样式设置字体样式

			// 第四.一步，创建表头的列
			for (int i = 0; i < columnNumber; i++)
			{
				HSSFCell cell = row.createCell(i);
				cell.setCellValue(columnName[i]);
				cell.setCellStyle(style);
			}

			// 第五步，创建单元格，并设置值
			for (int i = 0; dataList != null && i < dataList.length; i++)
			{
				row = sheet.createRow((int) i + 2);
				// 为数据内容设置特点新单元格样式1 自动换行 上下居中
				HSSFCellStyle zidonghuanhang = wb.createCellStyle();
				zidonghuanhang.setWrapText(true);// 设置自动换行
				zidonghuanhang.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER); // 创建一个居中格式

				// 设置边框
				zidonghuanhang.setBottomBorderColor(HSSFColor.BLACK.index);
				zidonghuanhang.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang.setBorderRight(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang.setBorderTop(HSSFCellStyle.BORDER_THIN);

				// 为数据内容设置特点新单元格样式2 自动换行 上下居中左右也居中
				HSSFCellStyle zidonghuanhang2 = wb.createCellStyle();
				zidonghuanhang2.setWrapText(true);// 设置自动换行
				zidonghuanhang2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER); // 创建一个上下居中格式
				zidonghuanhang2.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 左右居中

				// 设置边框
				zidonghuanhang2.setBottomBorderColor(HSSFColor.BLACK.index);
				zidonghuanhang2.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang2.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang2.setBorderRight(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang2.setBorderTop(HSSFCellStyle.BORDER_THIN);
				HSSFCell datacell = null;
				for (int j = 0; j < columnNumber; j++)
				{
					datacell = row.createCell(j);
					datacell.setCellValue(dataList[i][j]);
					datacell.setCellStyle(zidonghuanhang2);
				}
			}

			if (out != null)
			{
				// 第六步，将文件存到浏览器设置的下载位置
				String filename = fileName + ".xls";
				// filename="abc.xls";
				try
				{
					wb.write(out);// 将数据写出去
					String str = "导出" + fileName + "成功！";
					System.out.println(str);
				}
				catch (Exception e)
				{
					e.printStackTrace();
					String str1 = "导出" + fileName + "失败！";
					System.out.println(str1);
				}
				finally
				{
					out.close();
				}
			}
			else
			{
				// 第六步，将文件存到指定位置
				try
				{
					FileOutputStream fout = new FileOutputStream("C:WWWW.xls");
					wb.write(fout);
					String str = "导出" + fileName + "成功！";
					System.out.println(str);
					fout.close();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					String str1 = "导出" + fileName + "失败！";
					System.out.println(str1);
				}
			}

		}
		else
		{
			System.out.println("列数目长度名称三个数组长度要一致");
		}

	}

    /**
     * 创建excel对象
     * 将数据写入workbook并返回，注意不再需要写入文件或其他操作
     * 或写入文件或传递流，由外部调用处理
     * @param sheets 需要写入的sheet
     * @return
     */
	public static HSSFWorkbook makeExcel(List<PoiSheet> sheets) {
		// 第一步，创建一个workbook，对应一个Excel文件
		HSSFWorkbook wb = new HSSFWorkbook();
		for (PoiSheet poiSheet : sheets) {
			int columnNumber = poiSheet.getColumnNumber();
			String[] columnName = poiSheet.getColumnName();
			String sheetName = poiSheet.getSheetName();
			String[][] dataList = poiSheet.getDataList();
			// 在webbook中添加一个sheet,对应Excel文件中的sheet
			HSSFSheet sheet = wb.createSheet(sheetName);
			//统一设置列宽
			sheet.setDefaultColumnWidth(20);
			// 创建第0行 也就是标题
			HSSFRow row1 = sheet.createRow((int) 0);
			// 设置标题的高度
			row1.setHeightInPoints(50);
			// 标题, 注意只有当sheet的title不为空时，设置sheet的标题
			String titleName = StringUtils.isBlank(poiSheet.getTitleName()) ? poiSheet.getSheetName() : poiSheet.getTitleName();
			// 创建标题的单元格样式style2以及字体样式headerFont1
			HSSFCellStyle style2 = wb.createCellStyle();
			style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			style2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
			style2.setFillForegroundColor(HSSFColor.LIGHT_TURQUOISE.index);
			style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			HSSFFont headerFont1 = (HSSFFont) wb.createFont(); // 创建字体样式
			headerFont1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 字体加粗
			headerFont1.setFontName("黑体"); // 设置字体类型
			headerFont1.setFontHeightInPoints((short) 15); // 设置字体大小
			style2.setFont(headerFont1); // 为标题样式设置字体样式

			HSSFCell cell1 = row1.createCell(0);// 创建标题第一列
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, columnNumber - 1)); // 合并列标题
			cell1.setCellValue(titleName); // 设置值标题
			cell1.setCellStyle(style2); // 设置标题样式


			// 创建第1行 也就是表头
			HSSFRow row = sheet.createRow((int) 1);
			row.setHeightInPoints(37);// 设置表头高度

			// 第四步，创建表头单元格样式 以及表头的字体样式
			HSSFCellStyle style = wb.createCellStyle();
			style.setWrapText(true);// 设置自动换行
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER); // 创建一个居中格式

			style.setBottomBorderColor(HSSFColor.BLACK.index);
			style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			style.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style.setBorderTop(HSSFCellStyle.BORDER_THIN);

			HSSFFont headerFont = (HSSFFont) wb.createFont(); // 创建字体样式
			headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 字体加粗
			headerFont.setFontName("黑体"); // 设置字体类型
			headerFont.setFontHeightInPoints((short) 10); // 设置字体大小
			style.setFont(headerFont); // 为标题样式设置字体样式

			// 第四.一步，创建表头的列
			for (int i = 0; i < columnNumber; i++) {
				HSSFCell cell = row.createCell(i);
				cell.setCellValue(columnName[i]);
				cell.setCellStyle(style);
			}

			// 第五步，创建单元格，并设置值
			for (int i = 0; dataList != null && i < dataList.length; i++) {
				row = sheet.createRow((int) i + 2);
				// 为数据内容设置特点新单元格样式1 自动换行 上下居中
				HSSFCellStyle zidonghuanhang = wb.createCellStyle();
				zidonghuanhang.setWrapText(true);// 设置自动换行
				zidonghuanhang.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER); // 创建一个居中格式

				// 设置边框
				zidonghuanhang.setBottomBorderColor(HSSFColor.BLACK.index);
				zidonghuanhang.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang.setBorderRight(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang.setBorderTop(HSSFCellStyle.BORDER_THIN);

				// 为数据内容设置特点新单元格样式2 自动换行 上下居中左右也居中
				HSSFCellStyle zidonghuanhang2 = wb.createCellStyle();
				zidonghuanhang2.setWrapText(true);// 设置自动换行
				zidonghuanhang2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER); // 创建一个上下居中格式
				zidonghuanhang2.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 左右居中

				// 设置边框
				zidonghuanhang2.setBottomBorderColor(HSSFColor.BLACK.index);
				zidonghuanhang2.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang2.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang2.setBorderRight(HSSFCellStyle.BORDER_THIN);
				zidonghuanhang2.setBorderTop(HSSFCellStyle.BORDER_THIN);
				HSSFCell datacell = null;
				for (int j = 0; j < columnNumber; j++) {
					datacell = row.createCell(j);
					datacell.setCellStyle(zidonghuanhang2);
					try {
						datacell.setCellValue(dataList[i][j]);
					} catch (Exception e) {
						datacell.setCellValue("");
						e.printStackTrace();
					}
				}
			}
		}
		return wb;
	}


	/**
	 *  初始化工作薄
	 * @param fileType  文件类型
	 * @param inStream  输入流
	 * @return Workbook 返回一个工作薄
	 * @throws IOException
	 */
	private static Workbook initWorkBook(String fileType, InputStream inStream) throws IOException
	{
		if (fileType != null)
			fileType = fileType.toLowerCase();
		else
			fileType = "";
		Workbook workbook = null;
		// 根据后缀，得到不同的Workbook子类，即HSSFWorkbook或XSSFWorkbook
		if (fileType.endsWith(SUFFIX_2003))
		{
			workbook = new HSSFWorkbook(inStream);
		}
		else if (fileType.endsWith(SUFFIX_2007))
		{
			workbook = new XSSFWorkbook(inStream);
		}
		return workbook;
	}

	/**
	 *  解析Excel 数据
	 * @param fileType  文件类型
	 * @param inStream  输入流
	 * @param beginRow  从哪一行开始读取
	 * @param columnList 定义每一个列的名字
	 * @return  返回数据列表集合
	 * @throws IOException  io读取异常
	 */
	public static  List<Map<String, Object>> dealExcel(String fileType, InputStream inStream, int beginRow, List<String> columnList) throws IOException
	{
		List<Map<String, Object>> reList = new ArrayList<Map<String, Object>>();

		if (columnList == null || columnList.size() <= 0)
			return reList;
		Workbook book = initWorkBook(fileType, inStream);
		int numOfSheet = book.getNumberOfSheets();
		if (numOfSheet >= 1)
		{
			Sheet sheet = book.getSheetAt(0);
			if (sheet != null)
			{
				int count = 0;
				Iterator<Row> iterator = sheet.iterator();
				while (iterator.hasNext())
				{
					Row row = iterator.next();

					// 由于第一行是标题，因此这里单独处理
					if (count >= beginRow - 1)
					{
						Map<String, Object> reMap = new HashMap<String, Object>();

						Iterator<Cell> citerator = row.iterator();
						int columnCount = 0;
						while (citerator.hasNext())
						{
							Cell cell = citerator.next();

							// 定义每一个cell的数据类型
							cell.setCellType(CellType.STRING);
							// 取出cell中的value
							String value = cell.getStringCellValue();
							try {
								reMap.put(columnList.get(columnCount), value);
							} catch (IndexOutOfBoundsException e) {
								//throw new IndexOutOfBoundsException("数据格式不合法");
								return null;
							}
							columnCount++;
						}
						if (reMap.size() > 0)
							reList.add(reMap);
					}
					count++;
				}
			}
		}
		return reList;
	}

	/**
	 *  测试用例
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		/*String sheetName = "用车统计表单";
		String titleName = "用车申请数据统计表";
		String fileName = "用车申请统计表单";
		int columnNumber = 3;
		int[] columnWidth = { 10, 20, 30 };
		String[][] dataList = { { "001", "2015-01-01", "IT" }, { "002", "2015-01-02", "市场部" }, { "003", "2015-01-03", "测试" } };
		String[] columnName = { "单号", "申请时间", "申请部门" };*/
		//exportExcel(sheetName, titleName, fileName, columnNumber, columnWidth, columnName, dataList, null);
		InputStream is = new FileInputStream(new File("D://students.xls"));
		List<String> columnList = new ArrayList<>();
		columnList.add("fullName");
		columnList.add("ethnicity");
		columnList.add("credentialsType");
		columnList.add("credentialsNumber");
		columnList.add("sexType");
		columnList.add("birthday");
		List<Map<String,Object>> dataList = PoiUtil.dealExcel("xls",is, 3, columnList);
		/*for (Map<String,Object> oneData:dataList) {

		}*/
		System.out.println(dataList);
	}

}
