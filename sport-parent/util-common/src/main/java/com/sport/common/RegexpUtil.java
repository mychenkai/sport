/***********************************************************************
 * Note: Console Application
 **********************************************************************/
 
/** 
 *
 * @author Chenyang
 * @date 
 * @copyright CCompass
 *
 */
package com.sport.common;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexpUtil
{
  private static final HashMap<String, Pattern> patternMap = new HashMap<String, Pattern>();
  public static final String CHINESE_PATTERN = "[一-龥]";

  public static boolean match(String patternStr, String str)
    throws Exception
  {
    Pattern pattern = null;
    if (!patternMap.containsKey(patternStr)) {
      pattern = Pattern.compile(patternStr);
      patternMap.put(patternStr, pattern);
    } else {
      pattern = (Pattern)patternMap.get(patternStr);
    }
    Matcher matcher = pattern.matcher(str);
    boolean b = matcher.matches();
    return b;
  }

  public static boolean contains(String patternStr, String str)
    throws Exception
  {
    Pattern pattern = null;
    boolean retFlag = false;
    if (!patternMap.containsKey(patternStr)) {
      pattern = Pattern.compile(patternStr);
      patternMap.put(patternStr, pattern);
    } else {
      pattern = (Pattern)patternMap.get(patternStr);
    }
    Matcher matcher = pattern.matcher(str);
    if (matcher.find()) {
      retFlag = true;
    }

    return retFlag;
  }

  public static void ipTest() {
    String patternStr = "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})";
    Pattern pattern = Pattern.compile(patternStr);
    Matcher matcher = pattern.matcher("20.100.1.1");
    boolean b = matcher.matches();

    System.out.println(b);
  }

  public static void main(String[] args) throws Exception {
    long startTime = System.currentTimeMillis();
    System.out.println(contains("(202)\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})", "20.100.1.1"));
    long endTime = System.currentTimeMillis();
    System.out.println(" it takes time:" + (endTime - startTime));
  }

  private static void chineseTest()
  {
  }

  private static void test()
  {
    String patternStr = "";
    Pattern pattern = Pattern.compile("正则表达式");
    Matcher matcher = pattern.matcher("正则表达式 Hello World,正则表达式 Hello World ");
    StringBuffer sbr = new StringBuffer();
    while (matcher.find()) {
      matcher.appendReplacement(sbr, "Java");
    }
    matcher.appendTail(sbr);
    System.out.println(sbr.toString());
  }
}
