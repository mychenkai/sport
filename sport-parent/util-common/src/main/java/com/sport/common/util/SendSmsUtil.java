package com.sport.common.util;

import java.io.IOException;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendSmsUtil extends Thread {
	
	/**
     * 日志输出类
     */
    private static Logger logger = LoggerFactory.getLogger(SendSmsUtil.class);
    
	/**
	 * 手机号码
	 */
	private String toMobile;
	/**
	 * 验证码
	 */
	private String verifictionCode;
	private String smsMessageText;
	
	public SendSmsUtil() {
		
	}
	
	public SendSmsUtil(String toMobile, String verifictionCode) {
		this.toMobile = toMobile;
		this.verifictionCode = verifictionCode;
		this.smsMessageText = "您的短信验证码是：" + verifictionCode +",请在1分钟之内正确输入！";
		logger.info("Mobile: " + toMobile + ", verifictionCode: " + verifictionCode);
	}
	
	public void setSmsMessageText(String _smsMessageText){
		this.smsMessageText = _smsMessageText;
	}
	
	@Override
	public void run() {
		HttpClient client = new HttpClient();
		
		logger.info("Begin send sms: " + toMobile + ", smsMessageText: " + smsMessageText);
		
		PostMethod post = new PostMethod("http://client.cloud.hbsmservice.com:8080/sms_send2.do");
		post.addRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=gbk");// 在头文件中设置转码
		NameValuePair[] data = { 
				new NameValuePair("corp_id", "zd1204"), 
				new NameValuePair("corp_pwd", "zdkj15"), 
				new NameValuePair("corp_service", "10690549323172"), 
				new NameValuePair("mobile", toMobile), 
				new NameValuePair("msg_content", this.smsMessageText), 
				new NameValuePair("corp_msg_id ", "短信Id"), 
				new NameValuePair("ext ", "扩展小号") 
		};
//		PostMethod post = new PostMethod("http://sms.hbsmservice.com:8080/sms_send2.do");
//		post.addRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=gbk");// 在头文件中设置转码
//		NameValuePair[] data = { 
//				new NameValuePair("corp_id", "zd1633"), 
//				new NameValuePair("corp_pwd", "mm2289"), 
//				new NameValuePair("corp_service", "1069003238683"), 
//				new NameValuePair("mobile", toMobile), 
//				new NameValuePair("msg_content", this.smsMessageText), 
//				new NameValuePair("corp_msg_id ", "短信Id"), 
//				new NameValuePair("ext ", "扩展小号") 
//		};
		post.setRequestBody(data);
		
		try {
			client.executeMethod(post);
		} catch (HttpException e1) {
			logger.debug("Error on send sms. ", e1);
			e1.printStackTrace();
		} catch (IOException e1) {
			logger.debug("Error on send sms.. ", e1);
			e1.printStackTrace();
		}
		Header[] headers = post.getResponseHeaders();
		int statusCode = post.getStatusCode();
		logger.debug("statusCode:" + statusCode);
		System.out.println("statusCode:" + statusCode);
		for (Header h : headers) {
			System.out.println(h.toString());
		}
		String result;
		try {
			result = new String(post.getResponseBodyAsString());
			System.out.println(result);
		} catch (IOException e) {
			logger.debug("Error on getResponseBodyAsString ", e);
			e.printStackTrace();
		}
		
		post.releaseConnection();
	}
	
	public static void main(String[] args) {
		SendSmsUtil s = new SendSmsUtil("*", "a");
		s.setSmsMessageText("Test");
		s.start();
	}
}
