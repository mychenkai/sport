package com.sport.common;

public class JsonResult {
	/**
	 * 系统级别
	 */
	private String status = SUCCESS_STATUS;
	/**
	 * 业务级别
	 */
	private String bizStatus = SUCCESS_STATUS;

	private String message = SUCCESS_MESSAGE;

	public static String ERROR_STATUS = "error";
	
	public static String SUCCESS_STATUS = "success";

	public static String NOT_LOGGED_STATUS = "notlogged";
	
	public static String ERROR_MESSAGE = "操作失败！";

	public static String SUCCESS_MESSAGE = "操作成功！";
	
	public static String NOT_LOGGED_MESSAGE = "未登录！";

	public Object data;

	public JsonResult() {

	}
	
	public void toSuccess() {
		this.message = SUCCESS_MESSAGE;
		this.status = SUCCESS_STATUS;
	}

	public JsonResult(String status, String message) {
		this.setStatus(status);
		this.setMessage(message);
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public JsonResult error(String message) {
		return setMessageStatus(message, ERROR_STATUS);
	}
	
	public JsonResult success(String message) {
		return setMessageStatus(message, SUCCESS_STATUS);
	}
	
	public JsonResult setMessageStatus(String message, String status) {
		this.status = status;
		this.message = message;
		return this;
	}

	public String getBizStatus() {
		return bizStatus;
	}

	public void setBizStatus(String bizStatus) {
		this.bizStatus = bizStatus;
	}

	public boolean isSuccess(){
		return SUCCESS_STATUS.equals(this.getStatus());
	}
	public static JsonResult successResult (){
		return new JsonResult(JsonResult.SUCCESS_STATUS, JsonResult.SUCCESS_MESSAGE);
	}

	public static JsonResult failedResult (){
		return failedResult(JsonResult.ERROR_MESSAGE);
	}
	
	public static JsonResult failedResult (String message){
		JsonResult result = new JsonResult(JsonResult.ERROR_STATUS, message);
		result.setBizStatus(JsonResult.ERROR_STATUS);
		return result;
	}
}
