package com.sport.common;

import com.alibaba.fastjson.JSONObject;
import com.sport.UserTokenVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BaseController {

	@Autowired
	protected HttpServletRequest request;

	@Autowired
	protected HttpServletResponse response;

	public String getCurrentUserName(){
        String authorization = request.getHeader("authorization");
        return authorization == null ? null : new String(Base64Utils.decodeFromString(authorization));
    }

	public UserTokenVo getCurrentUserToken(){
        String authorizationInfo = request.getHeader("authorization_info");
        if(authorizationInfo != null) {
        	try{
	        	String tokenJson = new String(Base64Utils.decodeFromString(authorizationInfo));
	        	UserTokenVo userTokenVo = (UserTokenVo)JSONObject.parse(tokenJson);
	        	return userTokenVo;
        	}catch(Exception ex) {
        		
        	}
        }
        return null;
    }
	
//	public long getCurrentUserId(){
//        String authorizationInfo = request.getHeader("authorization_info");
//        if(authorizationInfo != null) {
//        	try{
//	        	String tokenJson = new String(Base64Utils.decodeFromString(authorizationInfo));
//	        	UserTokenVo userTokenVo = (UserTokenVo)JSONObject.parse(tokenJson);
//	        	return userTokenVo.getId();
//        	}catch(Exception ex) {
//        		
//        	}
//        }
//        return 0L;
//    }
	
	public String getCurrentToken() {
		return request.getHeader("token");
	}
	
	public Long getCurrentUserId() {
		return StringUtils.isBlank(request.getHeader("userid")) ? null : Long.valueOf(request.getHeader("userid"));
	}
	
	@InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat.setLenient(false);
        CustomDateEditor dateEditor = new CustomDateEditor(simpleDateFormat, true);
        binder.registerCustomEditor(Date.class,dateEditor);
    }
	
}
