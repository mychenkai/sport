package com.sport.common;

import java.util.Date;

public class TrainUtil {
	public static RunStatusBean[] trainStatusList = new RunStatusBean[]{
			new RunStatusBean(0, "待报名",   "待报名"),
			new RunStatusBean(1, "报名中",   "报名中"),
			new RunStatusBean(2, "未开始",   "未开始"),
			new RunStatusBean(3, "培训中",   "培训中"),
			new RunStatusBean(4, "已结束",   "已结束")
	};
	
	/**
	 * 根据当前时间，计算培训的进展状态信息
	 * @param nowDate
	 * @param signupStartDate
	 * @param signupEndDate
	 * @param trainStartDate
	 * @param trainEndDate
	 * @return
	 */
	public static RunStatusBean calcTrainingStatus(String nowDate, String signupStartDate, String signupEndDate, 
			String trainStartDate, String trainEndDate) {
		nowDate = nowDate.substring(0, 10);
		trainEndDate = trainEndDate.substring(0, 10);
		trainStartDate = trainStartDate.substring(0, 10);
		signupEndDate = signupEndDate.substring(0, 10);
		signupStartDate = signupStartDate.substring(0, 10);
		if(nowDate.compareTo(trainEndDate) > 0) {
			return trainStatusList[4];
		}
		if(nowDate.compareTo(trainStartDate) >= 0) {
			return trainStatusList[3];
		}
		if(nowDate.compareTo(signupEndDate) >= 0) {
			return trainStatusList[2];
		}
		if(nowDate.compareTo(signupStartDate) >= 0) {
			return trainStatusList[1];
		}
		return trainStatusList[0];
	}
	
	/**
	 *根据当前时间，计算培训的进展状态信息
	 * @param nowDate
	 * @param signupStartDate
	 * @param signupEndDate
	 * @param trainStartDate
	 * @param trainEndDate
	 * @return
	 */
	public static RunStatusBean calcTrainingStatus(Date nowDate, Date signupStartDate, Date signupEndDate, 
			Date trainStartDate, Date trainEndDate) {
		if(nowDate.getTime() > trainEndDate.getTime()) {
			return trainStatusList[4];
		}
		if(nowDate.getTime() > trainStartDate.getTime()) {
			return trainStatusList[3];
		}
		if(nowDate.getTime() > signupEndDate.getTime()) {
			return trainStatusList[2];
		}
		if(nowDate.getTime() > signupStartDate.getTime()) {
			return trainStatusList[1];
		}
		return trainStatusList[0];
	}
}
