package com.sport.common;

/** 
 * <pre>项目名称：shop-admin    
 * 类名称：SystemEnum    
 * 类描述：    
 * 创建人：陈凯 Mr_Chen__K@163.com  
 * 创建时间：2018年10月19日 下午2:37:13    
 * 修改人：陈凯 Mr_Chen__K@163.com   
 * 修改时间：2018年10月19日 下午2:37:13    
 * 修改备注：       
 * @version </pre>    
 */
public enum SystemEnum {



	FIND_EMPINFO_ERROR(1013,"查询信息失败"),
	CODE_TIMEOUT(1012,"验证码过期"),
	LOGIN_FAIL_ERROR(1011,"非法登陆"),
	ERROR_IS_LIMIT(1010,"接口限流"),
	ERROR_IS_ATTACK(1009,"非法攻击"),
	SIGN_IS_ERROR(1008,"签名无效"),
	APPSECRET_IS_EMPTY(1008,"appSecret无效"),
	TOKEN_TIME_OUT(1007,"请求超时"),
	HEARD_INFO_MISS(1006,"头信息不完整"),
	LOGIN_USER_LOCK(1005,"用户已锁定，请联系管理员进行解锁"),
	LOGIN_PASSWORD_ERROR(1004,"密码错误"),
	LOGIN_PASSWORD_ERROR_NUM(1003,"密码输错三次，该用户已锁定,请联系管理员进行解锁"),
	LOGIN_CODE_ERROR(1002,"验证码已失效,请刷新后重试"),
	LOGIN_USERNAME_NOT_EXITS(1001,"用户不存在"),
	SMS_MOBILE_EMPTY(2000,"手机号不可为空"),
	SMS_MOBILE_ERROR(2001,"请输入11位有效手机号"),
	SMS_CODE_TIMEOUT(2002,"请重新发送验证码,并重新输入验证码"),
	SMS_CODE_ERROR(2003,"验证码错误"),
	USERNAME_IS_EMPTY(2004,"请输入用户名"),
	USERNAME_IS_EXISTS(2005,"用户名已存在"),
	LOGIN_INFO_ERROR(1000,"信息不完整,请完善信息再登陆");



	private int code;
	private String message;
	
	private SystemEnum(int code,String message){
		this.code=code;
		this.message=message;
	}
	public int getCode() {
		return code;
	}
	public String getMessage() {
		return message;
	}
}
