package com.sport.common;

/**
 * <pre>项目名称：
 * 接口名称：IEnum
 * 接口描述：
 * 创建人：陈凯 Mr_Chen__K@163.com
 * 创建时间：2018/11/25 18:46
 * 修改人：陈凯 Mr_Chen__K@163.com
 * 修改时间：2018/11/25 18:46
 * 修改备注：
 * @version </pre>
 */
public interface IEnum {
    public int getCode();
    public String getMessage();
}
