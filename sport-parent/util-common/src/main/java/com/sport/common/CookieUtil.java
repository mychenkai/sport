package com.sport.common;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
public class CookieUtil {

    public static void writeCookie(String name, String value, String doman, int maxAge, HttpServletResponse response){
        //根据名字和值生成一个cookie对象
        Cookie cookie = new Cookie(name, value);
        //设置域名
        cookie.setDomain(doman);
        //设置根路径
        cookie.setPath("/");
        //判断是会话cookie还是持久化cookie
        if(maxAge>0){
            cookie.setMaxAge(maxAge);
        }
        //通过response将cookie从后台响应给前台
        response.addCookie(cookie);
    }

    public static String readCookie(HttpServletRequest request,String name){
        //通过request从前台获得cookie，得到的是个数组cookie
        Cookie[] cookies = request.getCookies();
        //判断是为了提高性能
        if (cookies==null){
            return "";
        }
        //遍历循环，地存储cookie的域名和当前访问的域名进行对比
        for (Cookie cookie : cookies) {
            if(name.equals(cookie.getName())){
                return cookie.getValue();
            }
        }
        return "";
    }
}
