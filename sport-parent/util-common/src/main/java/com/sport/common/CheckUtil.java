package com.sport.common;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public final class CheckUtil
{
  public static final String Mobile_regexp = "^[1][3,4,5,7,8][0-9]{9}$";
  public static final String icon_regexp = "^(/{0,1}\\w){1,}\\.(gif|dmp|png|jpg)$|^\\w{1,}\\.(gif|dmp|png|jpg)$";
  public static final String email_regexp = "(?:\\w[-._\\w]*\\w@\\w[-._\\w]*\\w\\.\\w{2,3}$)";
  public static final String url_regexp = "(\\w+)://([^/:]+)(:\\d*)?([^#\\s]*)";
  public static final String http_regexp = "(http|https|ftp)://([^/:]+)(:\\d*)?([^#\\s]*)";
  public static final String date_regexp = "^((((19){1}|(20){1})d{2})|d{2})[-\\s]{1}[01]{1}d{1}[-\\s]{1}[0-3]{1}d{1}$";
  public static final String phone_regexp = "^(?:0[0-9]{2,3}[-\\s]{1}|\\(0[0-9]{2,4}\\))[0-9]{6,8}$|^[1-9]{1}[0-9]{5,7}$|^[1-9]{1}[0-9]{10}$";
  public static final String ID_card_regexp = "^\\d{10}|\\d{13}|\\d{15}|\\d{18}$";
  public static final String ZIP_regexp = "^[0-9]{6}$";
  public static final String non_special_char_regexp = "^[^'\"\\;,:-<>\\s].+$";
  public static final String china_integer_english = "^[a-zA-Z0-9一-龻-*]+$";
  public static final String china_integer_english_punct = "^[a-zA-Z0-9一-龥\\,\\;\\.\\!\\?\\，\\；\\。\\！\\？]+$";
  public static final String letter_number_regexp = "^[A-Za-z0-9]+$";
  public static final String TIME_REGEXP_Y_M_D_O_M = "((19){1}|(20){1})\\d{2}-((0[1-9])|(1[0-2]))-((0[1-9])|([12][0-9])|(3[01])) (([01][0-9])|(2[0-3])):([0-5][0-9])";
  public static final String DATE_REGEXP_Y_M = "((19){1}|(20){1})\\d{2}-((0[1-9])|(1[0-2]))";
  public static final String DATE_REGEXP_Y_M_D = "((19){1}|(20){1})\\d{2}-((0[1-9])|(1[0-2]))-((0[1-9])|([12][0-9])|(3[01]))";

  public static boolean checkMobile(String phoneNum)
  {
    if (phoneNum.trim().equals("")) {
      return false;
    }
    return true;
  }

  public static boolean checkMobile(String phoneNum, String name)
  {
    if (phoneNum.trim().equals("")) {
      return false;
    }
    return true;
  }

  public static boolean checkIP(String phoneNum)
  {
    Pattern p1 = Pattern.compile("^(\\d+)\\.(\\d+)\\.(\\d+)\\.(\\d+)$");
    Matcher m1 = p1.matcher(phoneNum);
    if (m1.matches()) {
      return true;
    }
    return false;
  }

  public static boolean beNumber(String phoneNum, String name)
  {
    Pattern p1 = Pattern.compile("^(\\d+)$");
    Matcher m1 = p1.matcher(phoneNum);
    if (m1.matches()) {
      return true;
    }
    return false;
  }

  public static boolean isPosInteger(String numStr)
  {
    try
    {
      return new Integer(numStr).intValue() > 0; } catch (Exception e) {
    }
    return false;
  }

  public static boolean beNumber(String phoneNum)
  {
    return Pattern.compile("^(\\d+)$").matcher(phoneNum).matches();
  }

  public static boolean checkImage(String str)
  {
    Pattern p1 = Pattern.compile("^(/{0,1}\\w){1,}\\.(gif|dmp|png|jpg)$|^\\w{1,}\\.(gif|dmp|png|jpg)$");
    Matcher m1 = p1.matcher(str);
    if (m1.matches()) {
      return true;
    }
    return false;
  }

  public static boolean checkEmail(String str)
  {
    Pattern p1 = Pattern.compile("(?:\\w[-._\\w]*\\w@\\w[-._\\w]*\\w\\.\\w{2,3}$)");
    Matcher m1 = p1.matcher(str);
    if (m1.matches()) {
      return true;
    }
    return false;
  }

  public static boolean checkURL(String str)
  {
    Pattern p1 = Pattern.compile("(\\w+)://([^/:]+)(:\\d*)?([^#\\s]*)");
    Matcher m1 = p1.matcher(str);
    if (m1.matches()) {
      return true;
    }
    return false;
  }

  public static boolean checkHttp(String str)
  {
    Pattern p1 = Pattern.compile("(http|https|ftp)://([^/:]+)(:\\d*)?([^#\\s]*)");
    Matcher m1 = p1.matcher(str);
    if (m1.matches()) {
      return true;
    }
    return false;
  }

  public static boolean checkDate(String str)
  {
    Pattern p1 = Pattern.compile("^((((19){1}|(20){1})d{2})|d{2})[-\\s]{1}[01]{1}d{1}[-\\s]{1}[0-3]{1}d{1}$");
    Matcher m1 = p1.matcher(str);
    if (m1.matches()) {
      return true;
    }
    return false;
  }

  public static boolean checkTelephone(String str)
  {
    Pattern p1 = Pattern.compile("^(?:0[0-9]{2,3}[-\\s]{1}|\\(0[0-9]{2,4}\\))[0-9]{6,8}$|^[1-9]{1}[0-9]{5,7}$|^[1-9]{1}[0-9]{10}$");
    Matcher m1 = p1.matcher(str);
    if (m1.matches()) {
      return true;
    }
    return false;
  }

  public static boolean checkTelephone(String str, String message)
  {
    Pattern p1 = Pattern.compile("^(?:0[0-9]{2,3}[-\\s]{1}|\\(0[0-9]{2,4}\\))[0-9]{6,8}$|^[1-9]{1}[0-9]{5,7}$|^[1-9]{1}[0-9]{10}$");
    Matcher m1 = p1.matcher(str);
    if (m1.matches()) {
      return true;
    }
    return false;
  }

  public static boolean checkIDCard(String str)
  {
    //Pattern p1 = Pattern.compile("^\\d{10}|\\d{13}|\\d{15}|\\d{18}$");
    Pattern p1 = Pattern.compile("\\d{15}(\\d{2}[0-9xX])?");
    Matcher m1 = p1.matcher(str);
    if (m1.matches()) {
      return true;
    }
    return false;
  }

  public static boolean checkZip(String str)
  {
    Pattern p1 = Pattern.compile("^[0-9]{6}$");
    Matcher m1 = p1.matcher(str);
    if (m1.matches()) {
      return true;
    }
    return false;
  }

  public static boolean checkNoSpecialChar(String str)
  {
    Pattern p1 = Pattern.compile("^[^'\"\\;,:-<>\\s].+$");
    Matcher m1 = p1.matcher(str);
    if (m1.matches()) {
      return true;
    }
    return false;
  }

  public static boolean checkSpecialChar(String str)
  {
    if ((str.contains("~")) || (str.contains("!")) || (str.contains("#")) || (str.contains("$")) || (str.contains("^")) || (str.contains("&")) || (str.contains("*")) || (str.contains("|"))) {
      return false;
    }
    return true;
  }

  public static boolean checkMobil(String str, String name)
  {
    String reg = "^(13[0-9]|15[^4]|18[6|8|9])\\d{8}$";
    Pattern pattern = Pattern.compile(reg);
    Matcher m1 = pattern.matcher(str);
    if (!m1.matches()) {
      return false;
    }
    return true;
  }

  public static boolean checkInputTime(String str)
  {
    Pattern p1 = Pattern.compile("((19){1}|(20){1})\\d{2}-((0[1-9])|(1[0-2]))-((0[1-9])|([12][0-9])|(3[01])) (([01][0-9])|(2[0-3])):([0-5][0-9])");
    Matcher m1 = p1.matcher(str);
    if (m1.matches()) {
      return true;
    }
    return false;
  }

  public static boolean checkInputDate(String str, String regx)
  {
    Pattern p1 = Pattern.compile(regx);
    Matcher m1 = p1.matcher(str);
    if (m1.matches()) {
      return true;
    }
    if (regx.equals("((19){1}|(20){1})\\d{2}-((0[1-9])|(1[0-2]))-((0[1-9])|([12][0-9])|(3[01]))"));
    return false;
  }

  public static boolean checkSMSLength(String smsContents)
  {
    int length = smsContents.length();
    try {
      if (RegexpUtil.contains("[一-龥]", smsContents)) {
        if (length <= 70) {
          return true;
        }
        return false;
      }
      if (length <= 140) {
        return true;
      }
      return false;
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }
  public static boolean checkMobileInfo(String mobile) {
		if (StringUtils.isBlank( mobile)) {
			return false;
		}
		Pattern p1 = Pattern.compile(Mobile_regexp);
		Matcher m1 = p1.matcher(mobile);
		if (m1.matches()) {
			return true;
		}
		return false;
  }

  /**
   * 检查id 是否为空
   * @param id
   * @return
   */
  public static  boolean isBlankId(Long id) {
    if (id ==null||StringUtils.isBlank(id.toString()) || id == 0L ) {
       return true;
    }
    return false;
  }

  /**
   * 判断是否为空 null
   * @param charSequence
   * @return
   */
  public static  boolean isBlank(CharSequence charSequence) {
    if (StringUtils.isBlank(charSequence) || charSequence == null || "null".equals(charSequence)) {
       return true;
    }
    return false;
  }

  /**
   * 判断integer 类型的值是否为空
   * @param value
   * @return
   */
  public static  boolean isIntegerBlank(Integer value) {
    if (value == null  || value == 0) {
      return true;
    }
    return false;
  }

  public static  boolean isIntegerNull(Integer value) {
    if (value == null) {
      return true;
    }
    return false;
  }

  /**
   * 判断返回状态是否为成功
   * @param jsonResult
   * @return
   */
  public static  boolean isSuccess(JsonResult jsonResult) {
    if (jsonResult == null) return false;
    if (JsonResult.SUCCESS_STATUS.equals(jsonResult.getStatus())) {
      return true;
    }
    return false;
  }


  public static void main(String[] args) {
    Integer a = new Integer("232.2");
  }
}