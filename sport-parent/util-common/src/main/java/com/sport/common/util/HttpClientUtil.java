package com.sport.common.util;

import com.google.gson.Gson;
import com.sport.common.ServerResponse;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

public class HttpClientUtil {

    public static void main(String[] args) {
       /*Map<String,String> map=new HashMap<String,String>();
        map.put("id",9+"");
        map.put("brandName","李宁");
        map.put("logo","123.gif");
        map.put("brandDescribe","李宁");
        String url="http://localhost:8080/brands";
        ServerResponse response = sendPut(url, map, null);
        System.out.println(response);*/

        /*String url="http://localhost:8082/brands";
        ServerResponse response = sendDelete(url, 8, null);
        System.out.println(response);*/

    }

    //发送删除请求
    public static ServerResponse sendDelete(String url, Integer id, Map<String,String> headers) {
        CloseableHttpClient client = HttpClientBuilder.create().build();
        //如果请求路径没有后缀则不需要添加.jhtml
        HttpDelete httpDelete = new HttpDelete(url+"/"+id+".jhtml");
        buildHeaders(headers, httpDelete);
        CloseableHttpResponse response=null;
        String string="";
        try {
            response = client.execute(httpDelete);
            string = EntityUtils.toString(response.getEntity(), "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            buildClose(client, response , null,null,null,httpDelete);
        }

        Gson gson = new Gson();
        return gson.fromJson(string,ServerResponse.class);
    }

   //发送修改请求
    public static ServerResponse sendPut(String url,Map<String,String> params,Map<String,String> headers){
        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpPut httpPut = new HttpPut(url);
        Gson gson = new Gson();
        if (params!=null && !params.isEmpty()){
            String jsonParams = gson.toJson(params);
            StringEntity entity = new StringEntity(jsonParams,"utf-8");
            entity.setContentType("application/json");
            httpPut.setEntity(entity);
        }
        //提取方法
        buildHeaders(headers, httpPut);
        CloseableHttpResponse response=null;
        String string="";
        try {
            response = client.execute(httpPut);
            string = EntityUtils.toString(response.getEntity(), "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            buildClose(client, response , null,null,httpPut,null);
        }

        return  gson.fromJson(string,ServerResponse.class);
    }


     // 关流
    private static void buildClose(CloseableHttpClient httpClient,  CloseableHttpResponse response, HttpGet httpGet,HttpPost httpPost,HttpPut httpPut,HttpDelete httpDelete) {
        if (null!=response){
            try {
                response.close();
                response=null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (null!=httpGet){
            httpGet.releaseConnection();
            httpGet=null;
        }
        if (null!=httpPost){
            httpPost.releaseConnection();
            httpPost=null;
        }
        if (null!=httpPut){
            httpPut.releaseConnection();
            httpPut=null;
        }
        if (null!=httpDelete){
            httpDelete.releaseConnection();
            httpDelete=null;
        }
        if (null!=httpClient){
            try {
                httpClient.close();
                httpClient=null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


     //给header赋值
    private static void buildHeaders(Map<String, String> headers, HttpRequest request) {
        if (headers!=null && !headers.isEmpty()){
            Iterator<Map.Entry<String, String>> iterator = headers.entrySet().iterator();
            if (iterator.hasNext()){
                Map.Entry<String, String> next = iterator.next();
                request.addHeader(next.getKey(),next.getValue());
            }
        }
    }


    //赋值body
    private static void buildParams(Map<String, String> params, List<NameValuePair> pairList) {
        if (params!=null && params.size()>0){
            Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator();
            while (iterator.hasNext()){
                Map.Entry<String, String> entry = iterator.next();
                pairList.add(new BasicNameValuePair(entry.getKey(),entry.getValue()));
            }
        }
    }
    //发送get请求
    public static String sendGet(String url, Map<String,String> params, Map<String, String> headers){
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = null;
        if(null!=params && params.size()>0) {
            //设置请求体
            List<NameValuePair> pairs = new ArrayList();
            Iterator<Map.Entry<String, String>> headersIterator = params.entrySet().iterator();
            while (headersIterator.hasNext()) {
                Map.Entry<String, String> map = headersIterator.next();
                String key = map.getKey();
                String value = map.getValue();
                pairs.add(new BasicNameValuePair(key, value));
            }
            try {
                String str = EntityUtils.toString(new UrlEncodedFormEntity(pairs, "utf-8"));
                httpGet = new HttpGet(url+"?"+str);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(null!=headers && headers.size()>0) {
            //设置请求头
            Iterator<Map.Entry<String, String>> iterator = headers.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, String> map = iterator.next();
                String key = map.getKey();
                String value = map.getValue();
                httpGet.addHeader(key, value);
            }
        }
        CloseableHttpResponse response = null;
        String result = "";
        try {
            response = httpClient.execute(httpGet);
            result = EntityUtils.toString(response.getEntity(), "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(null != response) {
                    response.close();
                    response=null;
                }
                if(null != httpGet) {
                    httpGet.releaseConnection();
                }
                if(null != httpClient) {
                    httpClient.close();
                    httpClient=null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    //发送post请求
    public static String sendPost(String url, Map<String,String> params,Map<String,String> heards){
        //打开浏览器
        CloseableHttpClient client = HttpClientBuilder.create().build();
        //发送post请求
        HttpPost httpPost = new HttpPost(url);
        //传递body参数
        if (null!=params && params.size()>0){
            List<NameValuePair> pairs=new ArrayList<NameValuePair>();
            Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator();
            while (iterator.hasNext()){
                Map.Entry<String, String> entry = iterator.next();
                String key = entry.getKey();
                String value = entry.getValue();
                pairs.add(new BasicNameValuePair(key,value));
            }
            UrlEncodedFormEntity urlEncodedFormEntity = null;
            try {
                urlEncodedFormEntity = new UrlEncodedFormEntity(pairs, "utf-8");
                httpPost.setEntity(urlEncodedFormEntity);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        //传递header参数
        if (null!=heards&&heards.size()>0){
            Iterator<Map.Entry<String, String>> entryIterator = heards.entrySet().iterator();
            while (entryIterator.hasNext()){
                Map.Entry<String, String> next = entryIterator.next();
                String key = next.getKey();
                String value = next.getValue();
                httpPost.addHeader(key,value);
            }
        }
        //按回车，得到响应结果
        String result="";
        CloseableHttpResponse response=null;
        try {
            response = client.execute(httpPost);
            HttpEntity entity = response.getEntity();
            result = EntityUtils.toString(entity,"utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (null!=response){
                try {
                    response.close();
                    response=null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null!=httpPost){
                httpPost.releaseConnection();
            }
            if (null!=client){
                try {
                    client.close();
                    client=null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
}
