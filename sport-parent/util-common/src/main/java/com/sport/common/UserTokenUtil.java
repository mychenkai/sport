package com.sport.common;

/**
 * <pre>项目名称：
 * 接口名称：UserTokenUtil
 * 接口描述：
 * 创建人：陈凯 Mr_Chen__K@163.com
 * 创建时间：2019/8/16 14:32
 * 修改人：陈凯 Mr_Chen__K@163.com
 * 修改时间：2019/8/16 14:32
 * 修改备注：
 * @version </pre>
 */
public interface UserTokenUtil {
    public static final String USER_TOKEN_LIST_KEY = "user_tokens";
    /**
     * 有效期，单位分钟
     */
    public static int termOfValidity = 1440;
    public static int redisTermOfValidity = 86400;
}
