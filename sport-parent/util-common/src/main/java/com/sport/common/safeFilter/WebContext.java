package com.sport.common.safeFilter;

import javax.servlet.http.HttpServletRequest;

public class WebContext {

	private static final ThreadLocal<HttpServletRequest> requestLocal=new ThreadLocal<HttpServletRequest>();
	
	public static void setRequest(HttpServletRequest request){
		requestLocal.set(request);
	}
	
	public static HttpServletRequest getRequest(){
		return requestLocal.get();
	}
	
	public static void remove(){
		requestLocal.remove();
	}
	
}
