package com.sport.common;

import java.math.BigDecimal;

public class DataConvertUtil {
	
	public static BigDecimal getBigDecimal(Double d) {
		if(d == null)
			return BigDecimal.valueOf(0);
		return BigDecimal.valueOf(d);
	}
	
	public static BigDecimal getBigDecimal(Integer n) {
		if(n == null)
			return BigDecimal.valueOf(0);
		return BigDecimal.valueOf(n);
	}
	
	public static BigDecimal getBigDecimal(Long n) {
		if(n == null)
			return BigDecimal.valueOf(0);
		return BigDecimal.valueOf(n);
	}
}
