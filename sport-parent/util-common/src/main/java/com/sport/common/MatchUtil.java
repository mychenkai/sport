package com.sport.common;

import java.math.BigDecimal;
import java.util.Date;

public class MatchUtil {
	public static RunStatusBean[] matchStatusList = new RunStatusBean[]{
			new RunStatusBean(0, "publish",    "待报名"),
			new RunStatusBean(1, "signup",     "报名中"),
			new RunStatusBean(2, "stopsignup", "未开赛"),
			new RunStatusBean(3, "matchstart", "比赛中"),
			new RunStatusBean(4, "matchend",   "已结束")
	};
	
	/**
	 * 根据当前时间，计算赛事的状态信息
	 * @param nowDate
	 * @param signupStartTime
	 * @param signupEndTime
	 * @param matchStartTime
	 * @param matchEndTime
	 * @return
	 */
	public static RunStatusBean calcMatchStatus(String nowDate, String signupStartTime, String signupEndTime, 
			String matchStartTime, String matchEndTime) {
		nowDate = nowDate.substring(0, 10);
		matchEndTime = matchEndTime.substring(0, 10);
		matchStartTime = matchStartTime.substring(0, 10);
		signupEndTime = signupEndTime.substring(0, 10);
		signupStartTime = signupStartTime.substring(0, 10);
		if(nowDate.compareTo(matchEndTime) > 0) {
			return matchStatusList[4];
		}
		if(nowDate.compareTo(matchStartTime) >= 0) {
			return matchStatusList[3];
		}
		if(nowDate.compareTo(signupEndTime) >= 0) {
			return matchStatusList[2];
		}
		if(nowDate.compareTo(signupStartTime) >= 0) {
			return matchStatusList[1];
		}
		return matchStatusList[0];
	}
	
	/**
	 * 根据当前时间，计算赛事的状态信息
	 * @param nowDate
	 * @param signupStartTime
	 * @param signupEndTime
	 * @param matchStartTime
	 * @param matchEndTime
	 * @return
	 */
	public static RunStatusBean calcMatchStatus(Date nowDate, Date signupStartTime, Date signupEndTime, 
			Date matchStartTime, Date matchEndTime) {
		if(nowDate.getTime() > matchEndTime.getTime()) {
			return matchStatusList[4];
		}
		if(nowDate.getTime() > matchStartTime.getTime()) {
			return matchStatusList[3];
		}
		if(nowDate.getTime() > signupEndTime.getTime()) {
			return matchStatusList[2];
		}
		if(nowDate.getTime() > signupStartTime.getTime()) {
			return matchStatusList[1];
		}
		return matchStatusList[0];
	}
	
    public static BigDecimal add(BigDecimal... adds){
        BigDecimal result = new BigDecimal(0);
        for(BigDecimal add : adds){
            result = result.add(add);
        }
        return result;
    }

    /**
     * 计算总页数
     * @param total 数据总量
     * @param row 一页显示条数
     * @return
     */
    public static int pageCount(int total, int row){
        int totalPages;//总页数
        totalPages = total / row;
        if (total % row != 0){
            totalPages ++;
        }
        return totalPages;
    }
}
