package com.sport.common.util;

/**
 * Sql 工具类
 */
public class SqlUtil {

    /**
     * Sql 防止Sql 语句注入
     */
    public static String trimSingleQuotes(String val) {
        if(val.length()>0) {
            return val.replaceAll("'", "''");
        }
        return  "";
    }
}
