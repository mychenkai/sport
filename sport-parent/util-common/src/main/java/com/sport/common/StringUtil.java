package com.sport.common;

import org.springframework.util.CollectionUtils;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StringUtil {
    /**
     * 字符串集合按照中文排序
     * a A b B z 爱 才
     * @param tmpList
     */
    public static void sortByChina(List<String> tmpList){
        if(!CollectionUtils.isEmpty(tmpList)){
            Comparator<Object> CHINA_COMPARE = Collator.getInstance(java.util.Locale.CHINA);
            Collections.sort(tmpList, CHINA_COMPARE);
        }
    }
}
