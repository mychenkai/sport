package com.sport.common;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

public class DateUtil {


	public static final String Y_M_D="yyyy-MM-dd";
	public static final String Y_M_D_H_m_s="yyyy-MM-dd HH:mm:ss";

	public static String convertDateToStr(Date date , String pattern){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		return simpleDateFormat.format(date);
	}

	public static Date convertStrToDate(String str , String pattern) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		try {
			return simpleDateFormat.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private static SimpleDateFormat positionDateformat = new SimpleDateFormat("yyyyMMddHHmmss");

	private static SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy-MM-dd");

	private static SimpleDateFormat hourMinuteSecondFormat = new SimpleDateFormat("HH:mm:ss");

	private static String regex = "^[1-9]\\d{3}\\-(0?[1-9]|1[0-2])\\-(0?[1-9]|[12]\\d|3[01])\\s*(0?[1-9]|1\\d|2[0-3])(\\:(0?[1-9]|[1-5]\\d)){2}$";

	public static boolean isDateFormat(String dateTime) {
		return dateTime.matches(regex);
	}

	private static Map<String, String> month = new HashMap<String, String>();

	static {
		month.put("JAN", "01");
		month.put("FEB", "02");
		month.put("MAR", "03");
		month.put("APR", "04");
		month.put("MAY", "05");
		month.put("JUN", "06");
		month.put("JUL", "07");
		month.put("AUG", "08");
		month.put("SEP", "09");
		month.put("OCT", "10");
		month.put("NOV", "11");
		month.put("DEC", "12");
	}

	public static String getReportTime(Date data) {
		SimpleDateFormat format = new SimpleDateFormat("yyMMddHHmmss");
		return format.format(data);
	}

	public static String getHourMinuteSecondString(Date date) {
		return hourMinuteSecondFormat.format(date);
	}


	public static Date getHourMinuteSecond(String date) {
		try {
			return hourMinuteSecondFormat.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String monthConvert(String monthString) {
		return month.get(monthString);
	}
	public static int getCurrentYear() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.YEAR);
	}
	
	public static String getNowTimeStr() {
		Calendar c = Calendar.getInstance();
		return dateformat.format(c.getTime());
	}
	public static Date getNowDate() {
		return new Date();
	}


	public static String getDateFormateString(Date date) {
		return dateformat.format(date);
	}

	public static Date getDate(String dateStr) {
		try {
			return dateformat.parse(dateStr);
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Date getSimpleDate(String dateStr) {
		try {
			return simpleDateformat.parse(dateStr);
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Date getPositionDate(String dateStr) {
		try {
			return positionDateformat.parse(dateStr);
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getSimpleDateFormateString(Date date) {
		return simpleDateformat.format(date);
	}

	public static Date addAnyHour(Date startDate, int hour) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(startDate);
		gc.add(Calendar.HOUR, hour);
		return gc.getTime();
	}

	public static Date addOneDay(Date startDate) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(startDate);
		gc.add(Calendar.DATE, 1);
		return gc.getTime();
	}

	public static Date addAnyDay(Date startDate, int day) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(startDate);
		gc.add(Calendar.DATE, day);
		return gc.getTime();
	}

	public static Date addAnySecond(Date startDate, int second) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(startDate);
		gc.add(Calendar.SECOND, second);
		return gc.getTime();
	}

	/**
	 * 查询两个日期直接的间隔月数
	 * 
	 * @param startDay
	 * @param endDay
	 * @return
	 */
	public static int getIntervalMonths(Date startDay, Date endDay) {
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		c1.setTime(startDay);
		c2.setTime(endDay);
		int result = c2.get(Calendar.MONTH) - c1.get(Calendar.MONTH);
		return result == 0 ? 1 : Math.abs(result);
	}

	/**
	 * 查询两个日期直接的间隔天数
	 * 
	 * @param startDay
	 * @param endDay
	 * @return
	 */
	public static int getIntervalDays(Date startDay, Date endDay) {
		if (startDay == null || endDay == null) {
			return 0;
		}
		long ei = endDay.getTime() - startDay.getTime();
		return (int) (ei / (1000 * 60 * 60 * 24));
	}

	/**
	 * 查询两个日期直接的间隔小时数
	 * 
	 * @param startDay
	 * @param endDay
	 * @return
	 */
	public static int getIntervalHours(Date startDay, Date endDay) {
		if (startDay == null || endDay == null) {
			return 0;
		}
		long ei = endDay.getTime() - startDay.getTime();
		return (int) (ei / (1000 * 60 * 60));
	}

	/**
	 * 查询两个日期直接的间隔分钟数
	 * 
	 * @param startDay
	 * @param endDay
	 * @return
	 */
	public static int getIntervalMinutes(Date startDay, Date endDay) {
		if (startDay == null || endDay == null) {
			return 0;
		}
		long ei = endDay.getTime() - startDay.getTime();
		return Math.abs((int) (ei / (1000 * 60)));
	}

	/**
	 * 查询两个日期直接的间隔秒
	 * 
	 * @param startDay
	 * @param endDay
	 * @return
	 */
	public static int getIntervalSeconds(Date startDay, Date endDay) {
		if (startDay == null || endDay == null) {
			return 0;
		}
		long ei = endDay.getTime() - startDay.getTime();
		return Math.abs((int) (ei / 1000));
	}

	/**
	 * 计算两个时间相差的毫秒值
	 * @param startDay
	 * @param endDay
	 * @return
	 */
	public static long getIntervalMilliseconds(Date startDay, Date endDay) {
		if (startDay == null || endDay == null) {
			return 0;
		}
		long ei = endDay.getTime() - startDay.getTime();
		return ei;
	}
	/**
	 * 计算两个时间相差的毫秒值
	 * @param startDayStr
	 * @param endDayStr
	 * @return
	 */
	public static long getIntervalMilliseconds(String startDayStr, String endDayStr) {
		if (CheckUtil.isBlank(startDayStr) || CheckUtil.isBlank(endDayStr)) {
			return 0L;
		}
		Date startDay =getDate(startDayStr);
		Date endDay = getDate(endDayStr);
		long ei = endDay.getTime() - startDay.getTime();
		return ei;
	}



	public static String getWeekOfDate(Date dt) {
		String[] weekDays = { "日", "一", "二", "三", "四", "五", "六" };
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (w < 0)
			w = 0;
		return weekDays[w];
	}

	public static Date getMonthFirstDay(Date theDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(theDate);
		calendar.set(Calendar.DAY_OF_MONTH, 1);// 设置为1号,当前日期既为本月第一天
		return calendar.getTime();
	}

	public static Date getMonthLastDay(Date theDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(theDate);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		return calendar.getTime();
	}

	/**
	 * 判断文本是否符合日期标注
	 * @param date  日期文本
	 * @return
	 */
	public static boolean isDate(String date){
		Pattern p = Pattern.compile("^(\\d{4})-(\\d{2})-(\\d{2})$");
		return p.matcher(date).matches();
	}

	/**y
	 * 判断文本是否符合日期标准  2018-10-10~2018-10-15
	 * @param date  日期文本
	 * @return
	 */
	public static boolean isRangeDate(String date){
		Pattern p = Pattern.compile("^(\\d{4})-(\\d{2})-(\\d{2})~(\\d{4})-(\\d{2})-(\\d{2})$");
		return p.matcher(date).matches();
	}

    /**
     * 计算年龄
     * @param birthDay 日期
     * @return
     */
	public static int getAge(String birthDay)
	{
		if (CheckUtil.isBlank(birthDay)) {
			return 0;
		}
		Date d1 = getSimpleDate(birthDay);
		return getAge(d1);
	}

	// 由出生日期获得年龄
	public static  int getAge(Date birthDay)
	{
		Calendar cal = Calendar.getInstance();
		if (cal.before(birthDay))
		{
			return -1;
		}
		int yearNow = cal.get(Calendar.YEAR);
		int monthNow = cal.get(Calendar.MONTH);
		int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
		cal.setTime(birthDay);
		int yearBirth = cal.get(Calendar.YEAR);
		int monthBirth = cal.get(Calendar.MONTH);
		int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
		int age = yearNow - yearBirth;
		if (monthNow <= monthBirth)
		{
			if (monthNow == monthBirth)
			{
				if (dayOfMonthNow < dayOfMonthBirth)
					age--;
			}
			else
			{
				age--;
			}
		}
		return age;
	}


	//获取某年某月的最后一天日期
	public static Date getEndMonthDate(int year, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month - 1, 1);
		int day = calendar.getActualMaximum(5);
		calendar.set(year, month - 1, day);
		return calendar.getTime();
	}

	//获取今年是哪一年
	public static Integer getNowYear() {
		Date date = new Date();
		GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
		gc.setTime(date);
		return Integer.valueOf(gc.get(1));
	}

	//获取本月是哪一月
	public static int getNowMonth() {
		Date date = new Date();
		GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
		gc.setTime(date);
		return gc.get(2) + 1;
	}



	//获取某个日期的结束时间
	public static Timestamp getDayEndTime(Date d) {
		Calendar calendar = Calendar.getInstance();
		if(null != d) {
			calendar.setTime(d);
		}
		calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return new Timestamp(calendar.getTimeInMillis());
	}

	//获取某个日期的开始时间
	public static Timestamp getDayStartTime(Date d) {
		Calendar calendar = Calendar.getInstance();
		if(null != d) {
			calendar.setTime(d);
		}
		calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return new Timestamp(calendar.getTimeInMillis());
	}
	//获取本年的开始时间
	public static Date getBeginDayOfYear() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, getNowYear());
		cal.set(Calendar.MONTH, Calendar.JANUARY);
		cal.set(Calendar.DATE, 1);
		return getDayStartTime(cal.getTime());
	}

	//获取本年的结束时间
	public static Date getEndDayOfYear() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, getNowYear());
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DATE, 31);
		return getDayEndTime(cal.getTime());
	}

	//获取本月的开始时间
	public static Date getBeginDayOfMonth() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(getNowYear(), getNowMonth() - 1, 1);
		return getDayStartTime(calendar.getTime());
	}

	//获取本月的结束时间
	public static Date getEndDayOfMonth() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(getNowYear(), getNowMonth() - 1, 1);
		int day = calendar.getActualMaximum(5);
		calendar.set(getNowYear(), getNowMonth() - 1, day);
		return getDayEndTime(calendar.getTime());
	}

	//获取本月的开始时间
	public static String getBeginDayOfMonthStr() {
		return getSimpleDateFormateString(getBeginDayOfMonth());
	}

	//获取本月的结束时间 Str
	public static String getEndDayOfMonthStr() {
		return getSimpleDateFormateString(getEndDayOfMonth());
	}

	//获取本年的开始时间
	public static String getBeginDayOfYearStr() {
		return getSimpleDateFormateString(getBeginDayOfYear());
	}

	//获取本年的结束时间
	public static String getEndDayOfYearStr() {
		return getSimpleDateFormateString(getEndDayOfYear());
	}


	public static String yearCompare(Date fromDate,Date toDate){
		Calendar  from  =  Calendar.getInstance();
		from.setTime(fromDate);
		Calendar  to  =  Calendar.getInstance();
		to.setTime(toDate);
		int fromYear = from.get(Calendar.YEAR);
		int fromMonth = from.get(Calendar.MONTH);
		int toYear = to.get(Calendar.YEAR);
		int toMonth = to.get(Calendar.MONTH);
		int year = toYear  -  fromYear;
		int month = toMonth  - fromMonth;
		//返回2位小数，并且四舍五入
		DecimalFormat df = new DecimalFormat("######0.0");
		return df.format(year + month / 12);
	}

	public static String yearCompare(String fromDate,String toDate){
		return yearCompare(getSimpleDate(fromDate),getSimpleDate(toDate));
	}
	
	
	/**
     *
     * @param date
     * @param dateFormat : e.g:yyyy-MM-dd HH:mm:ss
     * @return
     */
	public static String formatDateByPattern(Date date, String dateFormat){
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		String formatTimeStr = null;
		if (date != null ) {
			formatTimeStr = sdf.format(date);
		}
		return formatTimeStr;
	}
	
	/**
	 * convert Date to cron ,eg.  "0 06 10 15 1 ? 2014"
	 * @param date : 时间点
	 * @return
	 */
	public static String getCronString(Date date) {
		String dateFormat="ss mm HH dd MM ? yyyy";
		return formatDateByPattern(date, dateFormat);
	}
}
