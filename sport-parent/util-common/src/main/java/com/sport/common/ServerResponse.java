package com.sport.common;

import java.io.Serializable;

/** 
 * <pre>项目名称：shop-admin    
 * 类名称：ServerResponse    
 * 类描述：    
 * 创建人：陈凯 Mr_Chen__K@163.com  
 * 创建时间：2018年10月18日 下午6:34:36    
 * 修改人：陈凯 Mr_Chen__K@163.com   
 * 修改时间：2018年10月18日 下午6:34:36    
 * 修改备注：       
 * @version </pre>    
 */
public class ServerResponse<T> implements Serializable{
	private static final long serialVersionUID = -8168529936197163804L;
	
	private Integer code;
	private String message;
	private T data;
	public ServerResponse(){
		
	}
	public ServerResponse(Integer code,String message,T data){
		this.code=code;
		this.message=message;
		this.data=data;
	}
	public static <T> ServerResponse<T> success(T data)
	{
		return new ServerResponse<T>(200,"ok",data);
	}
	public static ServerResponse success()
	{
		return new ServerResponse(200,"ok",null);
	}
	public static ServerResponse error(IEnum systemEnum){
		return new ServerResponse(systemEnum.getCode(),systemEnum.getMessage(),null);
	}
	public static ServerResponse error(){
		return new ServerResponse(-1,"出现异常,",null);
	}
	public Integer getCode() {
		return code;
	}
	public String getMessage() {
		return message;
	}
	public T getData() {
		return data;
	}
}
