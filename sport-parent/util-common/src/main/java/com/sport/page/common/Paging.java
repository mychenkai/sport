package com.sport.page.common;

import java.io.Serializable;
import java.util.List;

public class Paging extends PageCommonFilter implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 总页数
	 */
	private Long pages = 0L;
	
	/**
	 * 总条目数
	 */
	private Long total = 0L;
	
	private List<?> rows;

	public Paging() {
		this(0L, 1L, 10L);
	}

	public Paging(Long offset, Long page, Long limit) {
		this.page = page;
		this.limit = limit;
		this.offset = offset;
	}
	
	public Paging(PageCommonFilter pageCommonFilter) {
		this.page = pageCommonFilter.getPage();
		this.limit = pageCommonFilter.getLimit();
		this.offset = pageCommonFilter.getOffset();
		this.type = pageCommonFilter.getType();
	}

	private void calculatePages() {
		total = total > 0L ? total : 0L;
		this.limit = this.limit > 0L ? this.limit : 10L;
		pages = total / limit + (total % limit == 0L ? 0L : 1L);
		page = pages >= 1L ? Math.min(Math.max(page, 1L), pages) : 1L;
		offset = (page - 1L) * limit;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
		calculatePages();
	}


	public Long getPages() {
		return pages;
	}

	public void setPages(Long pages) {
		this.pages = pages;
	}

	public Long getStart() {
		return Math.max((page - 1L) * limit, 0L);
	}

	public List<?> getRows() {
		return rows;
	}

	public void setRows(List<?> rows) {
		this.rows = rows;
	}

}
