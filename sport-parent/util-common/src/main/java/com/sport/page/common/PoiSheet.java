package com.sport.page.common;

import java.util.List;
import java.util.Map;

/**
 * poi的shee对象
 */
public class PoiSheet {
    /**
     * excel中的sheet名称
     */
    private String sheetName;
    /**
     * excel标题
     */
    private String titleName;
    /**
     * excel文件名称
     */
    private String fileName;
    /**
     * 列数
     */
    private int columnNumber;
    /**
     * 每列宽度
     */
    private int[] columnWidth;
    /**
     * 每列名称
     */
    private String[] columnName;
    /**
     * 待写入的数据
     */
    private String[][] dataList;
    private Map<String, List<String>> combineMap;

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getColumnNumber() {
        return columnNumber;
    }

    public void setColumnNumber(int columnNumber) {
        this.columnNumber = columnNumber;
    }

    public int[] getColumnWidth() {
        return columnWidth;
    }

    public void setColumnWidth(int[] columnWidth) {
        this.columnWidth = columnWidth;
    }

    public String[] getColumnName() {
        return columnName;
    }

    public void setColumnName(String[] columnName) {
        this.columnName = columnName;
    }

    public String[][] getDataList() {
        return dataList;
    }

    public void setDataList(String[][] dataList) {
        this.dataList = dataList;
    }

    public Map<String, List<String>> getCombineMap() {
        return combineMap;
    }

    public void setCombineMap(Map<String, List<String>> combineMap) {
        this.combineMap = combineMap;
    }

    /**
     * 返回空的excel
     * @return
     */
    public static PoiSheet defaultSheet(){
        PoiSheet sheet = new PoiSheet();
        sheet.setSheetName("未参加比赛");
        sheet.setTitleName("请检查是否已经维护组员信息,是否参加过项目");
        sheet.setDataList(null);
        sheet.setColumnName(new String[]{"组别","组别"});
        sheet.setColumnNumber(2);

        return sheet;
    }
}
