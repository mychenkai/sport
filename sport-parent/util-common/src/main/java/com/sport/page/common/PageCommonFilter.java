package com.sport.page.common;

/**
 * @author thinkpad
 *
 */
public class PageCommonFilter {

	/**
	 * 当前页码, 默认为第1页
	 */
	protected Long page = 1L;

	/**
	 * 每页记录数量, 默认为10条
	 */
	protected Long limit = 10L;

	/**
	 * 分页开始的记录偏移量，数据所在位置
	 */
	protected Long offset = 0L;

	/**
	 * 请求分页时，页码的传递方式取"page"或者"offset"
	 * 分页方式:
	 *    offset: 按偏移方式, 
	 *    page: 按页码方式 ,
	 *    alldata: 不分页，获取所有数据
	 */
	protected String type = "offset";

	/**
	 * @return
	 */
	public Long getPage() {
		if ("offset".equals(type)) {
			if (limit == null)
				limit = 10L;
			if (offset == null)
				offset = 0L;
			return (offset / limit) + 1;
		}
		return page;
	}

	/**
	 * @param page 当前页码
	 */
	public void setPage(Long page) {
		this.page = page;
	}

	/**
	 * @return 每页记录数量
	 */
	public Long getLimit() {
		return limit;
	}

	/**
	 * 
	 * @param limit 每页记录数量
	 */
	public void setLimit(Long limit) {
		this.limit = limit;
	}

	/**
	 * 
	 * @return 分页开始记录数
	 */
	public Long getOffset() {
		if ("page".equals(type)) {
			return (page - 1) * limit;
		}
		return offset;
	}

	/**
	 * 
	 * @param offset 分页开始记录数
	 */
	public void setOffset(Long offset) {
		this.offset = offset;
	}

	
	public final static String PAGE_TYPE_ALL_DATA = "alldata";
	/**
	 * @return 分页方式, offset: 按偏移方式, page: 按页码方式 
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type 分页方式, offset: 按偏移方式, page: 按页码方式 , alldata: 不分页
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	public void noPaging() {
		this.type = PAGE_TYPE_ALL_DATA;
	}
	
	/**
	 * @return 是否获取所有数据
	 */
	public boolean isAllData(){
		return "alldata".equalsIgnoreCase(this.type);
	}

}
